﻿// <copyright file="RepoClasses.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Pre_BuiltPCs.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Pre_BuiltPCs.Data;

    /// <summary>
    /// Base interface, containing every CRUD method.
    /// </summary>
    /// <typeparam name="T"> Every table inherits it. </typeparam>
    public interface IRepository<T>
            where T : class
    {
        /// <summary>
        /// Adds an entity to the specified table.
        /// </summary>
        /// <param name="obj"> new entity. </param>
        void CreateItem(T obj);

        /// <summary>
        /// Picks one object from the table. (criteria: ID).
        /// </summary>
        /// <param name="id"> search criteria. </param>
        /// <returns> specified table's object. </returns>
        T GetOne(int id);

        /// <summary>
        /// Lists all entities in a table.
        /// </summary>
        /// <returns> Every entity in a table. </returns>
        List<T> GetAll();

        /// <summary>
        /// Modifies the specified entity's name.
        /// </summary>
        /// <param name="id"> specified entity's ID. </param>
        /// <param name="newValue"> new name. </param>
        void UpdateName(int id, string newValue);

        /// <summary>
        /// Modifies the specified entity's price.
        /// </summary>
        /// <param name="id"> specified entity's ID. </param>
        /// <param name="newValue"> new price. </param>
        void UpdatePrice(int id, int newValue);

        /// <summary>
        /// Deletes the specified entity.
        /// </summary>
        /// <param name="id"> specified ID. </param>
        void DeleteItem(int id);
    }

    /// <summary>
    /// Interface of CPU class. Contains specific methods.
    /// </summary>
    public interface ICPURepository : IRepository<CPU>
    {
        /// <summary>
        /// Modifies the specified entity's core amount.
        /// </summary>
        /// <param name="id"> specified entity's ID. </param>
        /// <param name="newValue"> new core amount. </param>
        void UpdateCores(int id, int newValue);
    }

    /// <summary>
    /// Interface of GPU class. Contains specific methods.
    /// </summary>
    public interface IGPURepository : IRepository<GPU>
    {
        /// <summary>
        /// Modifies the specified entity's VR capability.
        /// </summary>
        /// <param name="id"> specified entity's ID. </param>
        /// <param name="newValue"> able to used with VR, or not. </param>
        void UpdateVR(int id, int newValue);
    }

    /// <summary>
    /// Interface of MOTHERBOARD class. Contains specific methods.
    /// </summary>
    public interface IMBRepository : IRepository<MOTHERBOARD>
    {
        /// <summary>
        /// Modifies the specified entity's VR supportability.
        /// </summary>
        /// <param name="id"> specified entity's ID. </param>
        /// <param name="newValue"> able to support VR, or not. </param>
        void UpdateVR(int id, int newValue);

        /// <summary>
        /// Modifies the specified entity's memory type.
        /// </summary>
        /// <param name="id"> specified entity's ID. </param>
        /// <param name="newValue"> new memory type. </param>
        void UpdateMemoryType(int id, string newValue);
    }

    /// <summary>
    /// Interface of PSU class. Contains specific methods.
    /// </summary>
    public interface IPSURepository : IRepository<PSU>
    {
        /// <summary>
        /// Modifies the specified entity's efficiency.
        /// </summary>
        /// <param name="id"> specified entity's ID. </param>
        /// <param name="newValue"> new efficiency percent. </param>
        void UpdateEfficiency(int id, int newValue);
    }

    /// <summary>
    /// Interface of RAM class. Contains specific methods.
    /// </summary>
    public interface IRAMRepository : IRepository<RAM>
    {
        /// <summary>
        /// Modifies the specified entity's type.
        /// </summary>
        /// <param name="id"> specified entity's ID. </param>
        /// <param name="newValue"> new type. </param>
        void UpdateRAMType(int id, string newValue);
    }

    /// <summary>
    /// Interface of PC class. Contains specific methods.
    /// </summary>
    public interface IPCRepository
    {
        /// <summary>
        /// Adds an entity to the PC table.
        /// </summary>
        /// <param name="obj"> new entity. </param>
        void CreateItem(PC_SWITCHTABLE obj);
    }

    /// <summary>
    /// Class that contains every CPU specific CRUD and non-CRUD methods.
    /// </summary>
    public class CPURepository : ICPURepository
    {
        private PCDBEntities db;

        /// <summary>
        /// Initializes a new instance of the <see cref="CPURepository"/> class.
        /// </summary>
        /// <param name="db"> Database. </param>
        public CPURepository(PCDBEntities db)
        {
            this.db = db;
        }

        /// <inheritdoc/>
        public void CreateItem(CPU obj)
        {
            this.db.CPU.Add(obj);
            this.db.SaveChanges();
        }

        /// <inheritdoc/>
        public void DeleteItem(int id)
        {
            this.db.CPU.Remove(this.GetOne(id));
            this.db.SaveChanges();
        }

        /// <inheritdoc/>
        public List<CPU> GetAll()
        {
            return this.db.CPU.ToList();
        }

        /// <inheritdoc/>
        public CPU GetOne(int id)
        {
            return this.db.CPU.Where(x => x.CPU_ID == id).FirstOrDefault(); // TODO: HÜLYE VAGYOOOK
        }

        /// <inheritdoc/>
        public void UpdateCores(int id, int newValue)
        {
            var cpu = this.GetOne(id);
            cpu.CORES = newValue;
            cpu.THREADS = cpu.CORES * 2;
            this.db.SaveChanges();
        }

        /// <inheritdoc/>
        public void UpdateName(int id, string newValue)
        {
            var cpu = this.GetOne(id);
            cpu.CPU_NAME = newValue;
            this.db.SaveChanges();
        }

        /// <inheritdoc/>
        public void UpdatePrice(int id, int newValue)
        {
            var cpu = this.GetOne(id);
            cpu.PRICE = newValue;
            this.db.SaveChanges();
        }
    }

    /// <summary>
    /// Class that contains every GPu specific CRUD and non-CRUD methods.
    /// </summary>
    public class GPURepository : IGPURepository
    {
        private readonly PCDBEntities db;

        /// <summary>
        /// Initializes a new instance of the <see cref="GPURepository"/> class.
        /// </summary>
        /// <param name="db"> Database. </param>
        public GPURepository(PCDBEntities db)
        {
            this.db = db;
        }

        /// <inheritdoc/>
        public void CreateItem(GPU obj)
        {
            this.db.GPU.Add(obj);
            this.db.SaveChanges();
        }

        /// <inheritdoc/>
        public void DeleteItem(int id)
        {
            this.db.GPU.Remove(this.GetOne(id));
            this.db.SaveChanges();
        }

        /// <inheritdoc/>
        public List<GPU> GetAll()
        {
            return this.db.GPU.ToList();
        }

        /// <inheritdoc/>
        public GPU GetOne(int id)
        {
            return this.db.GPU.Where(x => x.GPU_ID == id).FirstOrDefault();
        }

        /// <inheritdoc/>
        public void UpdateName(int id, string newValue)
        {
            var gpu = this.GetOne(id);
            gpu.GPU_NAME = newValue;
            this.db.SaveChanges();
        }

        /// <inheritdoc/>
        public void UpdatePrice(int id, int newValue)
        {
            var gpu = this.GetOne(id);
            gpu.PRICE = newValue;
            this.db.SaveChanges();
        }

        /// <inheritdoc/>
        public void UpdateVR(int id, int newValue)
        {
            var gpu = this.GetOne(id);
            gpu.VR_READY = newValue;
            this.db.SaveChanges();
        }
    }

    /// <summary>
    /// Class that contains every MOTHERBOARD specific CRUD and non-CRUD methods.
    /// </summary>
    public class MBRepository : IMBRepository
    {
        private readonly PCDBEntities db;

        /// <summary>
        /// Initializes a new instance of the <see cref="MBRepository"/> class.
        /// </summary>
        /// <param name="db"> Database. </param>
        public MBRepository(PCDBEntities db)
        {
            this.db = db;
        }

        /// <inheritdoc/>
        public void CreateItem(MOTHERBOARD obj)
        {
            this.db.MOTHERBOARD.Add(obj);
            this.db.SaveChanges();
        }

        /// <inheritdoc/>
        public void DeleteItem(int id)
        {
            this.db.MOTHERBOARD.Remove(this.GetOne(id));
            this.db.SaveChanges();
        }

        /// <inheritdoc/>
        public List<MOTHERBOARD> GetAll()
        {
            return this.db.MOTHERBOARD.ToList();
        }

        /// <inheritdoc/>
        public MOTHERBOARD GetOne(int id)
        {
            return this.db.MOTHERBOARD.Where(x => x.MD_ID == id).FirstOrDefault();
        }

        /// <inheritdoc/>
        public void UpdateMemoryType(int id, string newValue)
        {
            var mb = this.GetOne(id);
            mb.MEMORY_TYPE = newValue;
            this.db.SaveChanges();
        }

        /// <inheritdoc/>
        public void UpdateName(int id, string newValue)
        {
            var mb = this.GetOne(id);
            mb.MB_NAME = newValue;
            this.db.SaveChanges();
        }

        /// <inheritdoc/>
        public void UpdatePrice(int id, int newValue)
        {
            var mb = this.GetOne(id);
            mb.PRICE = newValue;
            this.db.SaveChanges();
        }

        /// <inheritdoc/>
        public void UpdateVR(int id, int newValue)
        {
            var mb = this.GetOne(id);
            mb.FORM_FACTOR = "cica"; // TODO: SQL Változtatása -> Legyen VR READY
            this.db.SaveChanges();
        }
    }

    /// <summary>
    /// Class that contains every PSU specific CRUD and non-CRUD methods.
    /// </summary>
    public class PSURepository : IPSURepository
    {
        private PCDBEntities db;

        /// <summary>
        /// Initializes a new instance of the <see cref="PSURepository"/> class.
        /// </summary>
        /// <param name="db"> Database. </param>
        public PSURepository(PCDBEntities db)
        {
            this.db = db;
        }

        /// <inheritdoc/>
        public void CreateItem(PSU obj)
        {
            this.db.PSU.Add(obj);
            this.db.SaveChanges();
        }

        /// <inheritdoc/>
        public void DeleteItem(int id)
        {
            this.db.PSU.Remove(this.GetOne(id));
            this.db.SaveChanges();
        }

        /// <inheritdoc/>
        public List<PSU> GetAll()
        {
            return this.db.PSU.ToList();
        }

        /// <inheritdoc/>
        public PSU GetOne(int id)
        {
            return this.db.PSU.Where(x => x.PSU_ID == id).FirstOrDefault();
        }

        /// <inheritdoc/>
        public void UpdateEfficiency(int id, int newValue)
        {
            var psu = this.GetOne(id);
            psu.EFFICIENCY = newValue;
            this.db.SaveChanges();
        }

        /// <inheritdoc/>
        public void UpdateName(int id, string newValue)
        {
            var psu = this.GetOne(id);
            psu.PSU_NAME = newValue;
            this.db.SaveChanges();
        }

        /// <inheritdoc/>
        public void UpdatePrice(int id, int newValue)
        {
            var psu = this.GetOne(id);
            psu.PRICE = newValue;
            this.db.SaveChanges();
        }
    }

    /// <summary>
    /// Class that contains every RAM specific CRUD and non-CRUD methods.
    /// </summary>
    public class RAMRepository : IRAMRepository
    {
        private readonly PCDBEntities db;

        /// <summary>
        /// Initializes a new instance of the <see cref="RAMRepository"/> class.
        /// </summary>
        /// <param name="db"> Database. </param>
        public RAMRepository(PCDBEntities db)
        {
            this.db = db;
        }

        /// <inheritdoc/>
        public void CreateItem(RAM obj)
        {
            this.db.RAM.Add(obj);
            this.db.SaveChanges();
        }

        /// <inheritdoc/>
        public void DeleteItem(int id)
        {
            this.db.RAM.Remove(this.GetOne(id));
            this.db.SaveChanges();
        }

        /// <inheritdoc/>
        public List<RAM> GetAll()
        {
            return this.db.RAM.ToList();
        }

        /// <inheritdoc/>
        public RAM GetOne(int id)
        {
            return this.db.RAM.Where(x => x.RAM_ID == id).FirstOrDefault();
        }

        /// <inheritdoc/>
        public void UpdateName(int id, string newValue)
        {
            var ram = this.GetOne(id);
            ram.RAM_NAME = newValue;
            this.db.SaveChanges();
        }

        /// <inheritdoc/>
        public void UpdatePrice(int id, int newValue)
        {
            var ram = this.GetOne(id);
            ram.PRICE = newValue;
            this.db.SaveChanges();
        }

        /// <inheritdoc/>
        public void UpdateRAMType(int id, string newValue)
        {
            var ram = this.GetOne(id);
            ram.RAM_TYPE = newValue;
            this.db.SaveChanges();
        }
    }

    /// <summary>
    /// Class that contains every PC specific CRUD and non-CRUD methods.
    /// </summary>
    public class PCRepository : IPCRepository
    {
        private readonly PCDBEntities db;

        /// <summary>
        /// Initializes a new instance of the <see cref="PCRepository"/> class.
        /// </summary>
        /// <param name="db"> Database. </param>
        public PCRepository(PCDBEntities db)
        {
            this.db = db;
        }

        /// <inheritdoc/>
        public void CreateItem(PC_SWITCHTABLE obj)
        {
            this.db.PC_SWITCHTABLE.Add(obj);
            this.db.SaveChanges();
        }
    }
}