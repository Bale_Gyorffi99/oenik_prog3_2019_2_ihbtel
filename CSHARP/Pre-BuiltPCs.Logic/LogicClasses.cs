﻿// <copyright file="LogicClasses.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Pre_BuiltPCs.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Pre_BuiltPCs.Data;
    using Pre_BuiltPCs.Repository;

    /// <summary>
    /// All table's must interface. contains CRUD methods.
    /// </summary>
    /// <typeparam name="T">Every taable.</typeparam>
    public interface ILogic<T>
            where T : class
    {
        /// <summary>
        /// Adds an entity to the table.
        /// </summary>
        /// <param name="obj"> T object. </param>
        void CreateItem(T obj);

        /// <summary>
        /// Returns specified entity.
        /// </summary>
        /// <param name="id"> specified entity's number. </param>
        /// <returns> T entity. </returns>
        T GetOne(int id);

        /// <summary>
        /// Returns a List of all entities in the table.
        /// </summary>
        /// <returns> List of entities. </returns>
        List<T> GetAll();

        /// <summary>
        /// Updates the specified entity's name.
        /// </summary>
        /// <param name="id"> specified entity's number. </param>
        /// <param name="newValue"> new name. </param>
        void UpdateName(int id, string newValue);

        /// <summary>
        /// Updates the specified entity's price.
        /// </summary>
        /// <param name="id"> specified entity's number. </param>
        /// <param name="newValue"> new price. </param>
        void UpdatePrice(int id, int newValue);

        /// <summary>
        /// Deletes entity by given id.
        /// </summary>
        /// <param name="id"> Id of the deletable entity. </param>
        void DeleteItem(int id);

        /// <summary>
        /// Looks for the last entity's ID.
        /// </summary>
        /// <returns> last entity's ID. </returns>
        decimal GetMaxIdx();
    }

    /// <summary>
    /// Interface of MOTHERBOARD class. Contains specific methods.
    /// </summary>
    public interface IMBLogic : ILogic<MOTHERBOARD>
    {
        /// <summary>
        /// Updates the specified entity's VR supportability.
        /// </summary>
        /// <param name="id"> specified entity's number. </param>
        /// <param name="newValue"> Does it support VR, or not? 0 or 1. </param>
        void UpdateVR(int id, int newValue);

        /// <summary>
        /// Updates the specified entity's memory type.
        /// </summary>
        /// <param name="id"> specified entity's number. </param>
        /// <param name="newValue"> new memory type. </param>
        void UpdateMemoryType(int id, string newValue);
    }

    /// <summary>
    /// Interface of CPU class. Contains specific methods.
    /// </summary>
    public interface ICPULogic : ILogic<CPU>
    {
        /// <summary>
        /// Updates the specified entity's core amount.
        /// </summary>
        /// <param name="id"> specified entity's number. </param>
        /// <param name="newValue"> new core amount. </param>
        void UpdateCores(int id, int newValue);
    }

    /// <summary>
    /// Interface of GPU class. Contains specific methods.
    /// </summary>
    public interface IGPULogic : ILogic<GPU>
    {
        /// <summary>
        /// Updates the specified entity's price.
        /// </summary>
        /// <param name="id"> specified entity's number. </param>
        /// <param name="newValue"> 0 - 1 -> not VR ready or VR ready. </param>
        void UpdateVR(int id, int newValue);
    }

    /// <summary>
    /// Interface of RAM class. Contains specific methods.
    /// </summary>
    public interface IRAMLogic : ILogic<RAM>
    {
        /// <summary>
        /// Updates the specified entity's type.
        /// </summary>
        /// <param name="id"> specified entity's number. </param>
        /// <param name="newValue"> new type. </param>
        void UpdateRAMType(int id, string newValue);
    }

    /// <summary>
    /// Interface of PSU class. Contains specific methods.
    /// </summary>
    public interface IPSULogic : ILogic<PSU>
    {
        /// <summary>
        /// Updates the specified entity's efficiency.
        /// </summary>
        /// <param name="id"> specified entity's number. </param>
        /// <param name="newValue"> new value of efficiency. </param>
        void UpdateEfficiency(int id, int newValue);
    }

    /// <summary>
    /// Interface of PC class. Contains specific methods.
    /// </summary>
    public interface IPCLogic
    {
        /// <summary>
        /// Creates a new entity.
        /// </summary>
        /// <param name="obj"> a PC_SWITCHTABLE object. </param>
        void AddPC(PC_SWITCHTABLE obj);
    }

    /// <summary>
    /// Class of MOTHERBOARD class. Contains specific methods.
    /// </summary>
    public class MBLogic : IMBLogic
    {
        private readonly IMBRepository mbRepo;

        /// <summary>
        /// Initializes a new instance of the <see cref="MBLogic"/> class.
        /// </summary>
        public MBLogic()
        {
            this.mbRepo = new MBRepository(new PCDBEntities());
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MBLogic"/> class.
        /// </summary>
        /// <param name="ogRepo"> IMBRepository object. </param>
        public MBLogic(IMBRepository ogRepo)
        {
            this.mbRepo = ogRepo;
        }

        /// <summary>
        /// Adds an entity to the table.
        /// </summary>
        /// <param name="obj"> MOTHERBOARD object. </param>
        public void CreateItem(MOTHERBOARD obj)
        {
            this.mbRepo.CreateItem(obj);
        }

        /// <summary>
        /// Deletes entity by given id.
        /// </summary>
        /// <param name="id"> Id of the deletable entity. </param>
        public void DeleteItem(int id)
        {
            this.mbRepo.DeleteItem(id);
        }

        /// <summary>
        /// Returns a List of all MOTHERBOARDs in the table.
        /// </summary>
        /// <returns> List of MOTHERBOARDs. </returns>
        public List<MOTHERBOARD> GetAll()
        {
            return this.mbRepo.GetAll();
        }

        /// <summary>
        /// Returns specified entity.
        /// </summary>
        /// <param name="id"> specified entity's number. </param>
        /// <returns> specified entity. </returns>
        public MOTHERBOARD GetOne(int id)
        {
            return this.mbRepo.GetOne(id);
        }

        /// <summary>
        /// Updates the specified entity's memory type.
        /// </summary>
        /// <param name="id"> specified entity's number. </param>
        /// <param name="newValue"> new memory type. </param>
        public void UpdateMemoryType(int id, string newValue)
        {
            this.mbRepo.UpdateMemoryType(id, newValue);
        }

        /// <summary>
        /// Updates the specified entity's name.
        /// </summary>
        /// <param name="id"> specified entity's number. </param>
        /// <param name="newValue"> new name. </param>
        public void UpdateName(int id, string newValue)
        {
            this.mbRepo.UpdateName(id, newValue);
        }

        /// <summary>
        /// Updates the specified entity's price.
        /// </summary>
        /// <param name="id"> specified entity's number. </param>
        /// <param name="newValue"> new price. </param>
        public void UpdatePrice(int id, int newValue)
        {
            this.mbRepo.UpdatePrice(id, newValue);
        }

        /// <summary>
        /// Updates the specified entity's VR supportability.
        /// </summary>
        /// <param name="id"> specified entity's number. </param>
        /// <param name="newValue"> Does it support VR, or not? 0 or 1. </param>
        public void UpdateVR(int id, int newValue)
        {
            this.mbRepo.UpdateVR(id, newValue);
        }

        /// <summary>
        /// Averages the actual table all entity's price.
        /// </summary>
        /// <returns> Average price. </returns>
        public decimal AVGPrice()
        {
            decimal sumPrice = 0;
            foreach (var item in this.mbRepo.GetAll())
            {
                sumPrice += item.PRICE.Value;
            }

            sumPrice /= this.mbRepo.GetAll().Count;
            return sumPrice;
        }

        /// <summary>
        /// filters all MOTHERBOARD by a certain form.
        /// </summary>
        /// <param name="formType"> form type. </param>
        /// <returns> filtered list. </returns>
        public List<MOTHERBOARD> FilterByForm(string formType)
        {
            List<MOTHERBOARD> filtered = new List<MOTHERBOARD>();
            foreach (var item in this.mbRepo.GetAll())
            {
                if (item.FORM_FACTOR.ToString() == formType)
                {
                    filtered.Add(item);
                }
            }

            return filtered;
        }

        /// <summary>
        /// Looks for the last entity's ID.
        /// </summary>
        /// <returns> last entity's ID. </returns>
        public decimal GetMaxIdx()
        {
            return this.mbRepo.GetAll().Last().MD_ID;
        }
    }

    /// <summary>
    /// Class of CPU. Implements ICPULogic interface. Contains methods that relies on the same repository class as the controller one.
    /// </summary>
    public class CPULogic : ICPULogic
    {
        private readonly ICPURepository cpuRepo;

        /// <summary>
        /// Initializes a new instance of the <see cref="CPULogic"/> class.
        /// </summary>
        public CPULogic()
        {
            this.cpuRepo = new CPURepository(new PCDBEntities());
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CPULogic"/> class.
        /// </summary>
        /// <param name="ogRepo"> ICPURepository object. </param>
        public CPULogic(ICPURepository ogRepo)
        {
            this.cpuRepo = ogRepo;
        }

        /// <summary>
        /// Adds an entity to the table.
        /// </summary>
        /// <param name="obj"> CPU object. </param>
        public void CreateItem(CPU obj)
        {
            this.cpuRepo.CreateItem(obj);
        }

        /// <summary>
        /// Deletes entity by given id.
        /// </summary>
        /// <param name="id"> Id of the deletable entity. </param>
        public void DeleteItem(int id)
        {
            this.cpuRepo.DeleteItem(id);
        }

        /// <summary>
        /// Returns a List of all Entities in the table.
        /// </summary>
        /// <returns> List of all Entities. </returns>
        public List<CPU> GetAll()
        {
            return this.cpuRepo.GetAll();
        }

        /// <summary>
        /// Returns specified entity.
        /// </summary>
        /// <param name="id"> specified entity's number. </param>
        /// <returns> specified entity. </returns>
        public CPU GetOne(int id)
        {
            return this.cpuRepo.GetOne(id);
        }

        /// <summary>
        /// Updates the specified entity's core amount.
        /// </summary>
        /// <param name="id"> specified entity's number. </param>
        /// <param name="newValue"> new core amount. </param>
        public void UpdateCores(int id, int newValue)
        {
            this.cpuRepo.UpdateCores(id, newValue);
        }

        /// <summary>
        /// Updates the specified entity's name.
        /// </summary>
        /// <param name="id"> specified entity's number. </param>
        /// <param name="newValue"> new name. </param>
        public void UpdateName(int id, string newValue)
        {
            this.cpuRepo.UpdateName(id, newValue);
        }

        /// <summary>
        /// Updates the specified entity's price.
        /// </summary>
        /// <param name="id"> specified entity's number. </param>
        /// <param name="newValue"> new price. </param>
        public void UpdatePrice(int id, int newValue)
        {
            this.cpuRepo.UpdatePrice(id, newValue);
        }

        /// <summary>
        /// Averages the actual table all entity's price.
        /// </summary>
        /// <returns> Average price. </returns>
        public decimal AVGPrice()
        {
            decimal sumPrice = 0;
            foreach (var item in this.cpuRepo.GetAll())
            {
                sumPrice += item.PRICE.Value;
            }

            sumPrice /= this.cpuRepo.GetAll().Count;
            return sumPrice;
        }

        /// <summary>
        /// Looks for the last entity's ID.
        /// </summary>
        /// <returns> last entity's ID. </returns>
        public decimal GetMaxIdx()
        {
            return this.cpuRepo.GetAll().Last().CPU_ID;
        }
    }

    /// <summary>
    /// Class of GPU. Implements IGPULogic interface. Contains methods that relies on the same repository class as the controller one.
    /// </summary>
    public class GPULogic : IGPULogic
    {
        private readonly IGPURepository gpuRepo;

        /// <summary>
        /// Initializes a new instance of the <see cref="GPULogic"/> class.
        /// </summary>
        public GPULogic()
        {
            this.gpuRepo = new GPURepository(new PCDBEntities());
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GPULogic"/> class.
        /// </summary>
        /// <param name="ogRepo"> IGPURepository object. </param>
        public GPULogic(IGPURepository ogRepo)
        {
            this.gpuRepo = ogRepo;
        }

        /// <summary>
        /// Adds an entity to the table.
        /// </summary>
        /// <param name="obj"> PSU object. </param>
        public void CreateItem(GPU obj)
        {
            this.gpuRepo.CreateItem(obj);
        }

        /// <summary>
        /// Deletes entity by given id.
        /// </summary>
        /// <param name="id"> Id of the deletable entity. </param>
        public void DeleteItem(int id)
        {
            this.gpuRepo.DeleteItem(id);
        }

        /// <summary>
        /// Returns a List of all PSU in the table.
        /// </summary>
        /// <returns> List of all PSU. </returns>
        public List<GPU> GetAll()
        {
            return this.gpuRepo.GetAll();
        }

        /// <summary>
        /// Returns specified entity.
        /// </summary>
        /// <param name="id"> specified entity's number. </param>
        /// <returns> specified entity. </returns>
        public GPU GetOne(int id)
        {
            return this.gpuRepo.GetOne(id);
        }

        /// <summary>
        /// Updates the specified entity's name.
        /// </summary>
        /// <param name="id"> specified entity's number. </param>
        /// <param name="newValue"> new name. </param>
        public void UpdateName(int id, string newValue)
        {
            this.gpuRepo.UpdateName(id, newValue);
        }

        /// <summary>
        /// Updates the specified entity's price.
        /// </summary>
        /// <param name="id"> specified entity's number. </param>
        /// <param name="newValue"> new price. </param>
        public void UpdatePrice(int id, int newValue)
        {
            this.gpuRepo.UpdatePrice(id, newValue);
        }

        /// <summary>
        /// Updates the specified entity's price.
        /// </summary>
        /// <param name="id"> specified entity's number. </param>
        /// <param name="newValue"> 0 - 1 -> not VR ready or VR ready. </param>
        public void UpdateVR(int id, int newValue)
        {
            this.gpuRepo.UpdateVR(id, newValue);
        }

        /// <summary>
        /// Averages the actual table all entity's price.
        /// </summary>
        /// <returns> Average price. </returns>
        public decimal AVGPrice()
        {
            decimal sumPrice = 0;
            foreach (var item in this.gpuRepo.GetAll())
            {
                sumPrice += item.PRICE.Value;
            }

            sumPrice /= this.gpuRepo.GetAll().Count;
            return sumPrice;
        }

        /// <summary>
        /// Filters gpu entities in 2 price range.
        /// </summary>
        /// <param name="gpuMinPrice"> lowest price. </param>
        /// <param name="gpuMaxPrice"> highest price. </param>
        /// <returns> filtered List. </returns>
        public List<GPU> FilterPriceBetween(int gpuMinPrice, int gpuMaxPrice)
        {
            List<GPU> filtered = new List<GPU>();
            foreach (var item in this.gpuRepo.GetAll())
            {
                if (item.PRICE > gpuMinPrice)
                {
                    if (item.PRICE < gpuMaxPrice)
                    {
                        filtered.Add(item);
                    }
                }
            }

            return filtered;
        }

        /// <summary>
        /// Looks for the last entity's ID.
        /// </summary>
        /// <returns> last entity's ID. </returns>
        public decimal GetMaxIdx()
        {
            return this.gpuRepo.GetAll().Last().GPU_ID;
        }
    }

    /// <summary>
    /// Class of RAM. Implements IRAMLogic interface. Contains methods that relies on the same repository class as the controller one.
    /// </summary>
    public class RAMLogic : IRAMLogic
    {
        private readonly IRAMRepository ramRepo;

        /// <summary>
        /// Initializes a new instance of the <see cref="RAMLogic"/> class.
        /// </summary>
        public RAMLogic()
        {
            this.ramRepo = new RAMRepository(new PCDBEntities());
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RAMLogic"/> class.
        /// </summary>
        /// <param name="ogRepo"> IRAMRepository object. </param>
        public RAMLogic(IRAMRepository ogRepo)
        {
            this.ramRepo = ogRepo;
        }

        /// <summary>
        /// Adds an entity to the table.
        /// </summary>
        /// <param name="obj"> RAM object. </param>
        public void CreateItem(RAM obj)
        {
            this.ramRepo.CreateItem(obj);
        }

        /// <summary>
        /// Deletes entity by given id.
        /// </summary>
        /// <param name="id"> Id of the deletable entity. </param>
        public void DeleteItem(int id)
        {
            this.ramRepo.DeleteItem(id);
        }

        /// <summary>
        /// Returns a List of all PSU in the table.
        /// </summary>
        /// <returns> List of all PSU. </returns>
        public List<RAM> GetAll()
        {
            return this.ramRepo.GetAll();
        }

        /// <summary>
        /// Returns specified entity.
        /// </summary>
        /// <param name="id"> specified entity's number. </param>
        /// <returns> specified entity. </returns>
        public RAM GetOne(int id)
        {
            return this.ramRepo.GetOne(id);
        }

        /// <summary>
        /// Updates the specified entity's name.
        /// </summary>
        /// <param name="id"> specified entity's number. </param>
        /// <param name="newValue"> new name. </param>
        public void UpdateName(int id, string newValue)
        {
            this.ramRepo.UpdateName(id, newValue);
        }

        /// <summary>
        /// Updates the specified entity's price.
        /// </summary>
        /// <param name="id"> specified entity's number. </param>
        /// <param name="newValue"> new price. </param>
        public void UpdatePrice(int id, int newValue)
        {
            this.ramRepo.UpdatePrice(id, newValue);
        }

        /// <summary>
        /// Updates the specified entity's type.
        /// </summary>
        /// <param name="id"> specified entity's number. </param>
        /// <param name="newValue"> new type. </param>
        public void UpdateRAMType(int id, string newValue)
        {
            this.ramRepo.UpdateRAMType(id, newValue);
        }

        /// <summary>
        /// Averages the actual table all entity's price.
        /// </summary>
        /// <returns> Average price. </returns>
        public decimal AVGPrice()
        {
            decimal sumPrice = 0;
            foreach (var item in this.ramRepo.GetAll())
            {
                sumPrice += item.PRICE.Value;
            }

            sumPrice /= this.ramRepo.GetAll().Count;
            return sumPrice;
        }

        /// <summary>
        /// Filters entities above the given speed.
        /// </summary>
        /// <param name="minSpeed"> given speed. </param>
        /// <returns> filtered List. </returns>
        public List<RAM> FilterBySpeed(int minSpeed)
        {
            List<RAM> filtered = new List<RAM>();
            foreach (var item in this.ramRepo.GetAll())
            {
                if (item.SPEED > minSpeed)
                {
                    filtered.Add(item);
                }
            }

            return filtered;
        }

        /// <summary>
        /// Looks for the last entity's ID.
        /// </summary>
        /// <returns> last entity's ID. </returns>
        public decimal GetMaxIdx()
        {
            return this.ramRepo.GetAll().Last().RAM_ID;
        }
    }

    /// <summary>
    /// Class of PSU. Implements IPSULogic interface. Contains methods that relies on the same repository class as the controller one.
    /// </summary>
    public class PSULogic : IPSULogic
    {
        private readonly IPSURepository psuRepo;

        /// <summary>
        /// Initializes a new instance of the <see cref="PSULogic"/> class.
        /// </summary>
        public PSULogic()
        {
            this.psuRepo = new PSURepository(new PCDBEntities());
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PSULogic"/> class.
        /// </summary>
        /// <param name="ogRepo"> IPSURepository object. </param>
        public PSULogic(IPSURepository ogRepo)
        {
            this.psuRepo = ogRepo;
        }

        /// <summary>
        /// Adds an entity to the table.
        /// </summary>
        /// <param name="obj"> PSU object. </param>
        public void CreateItem(PSU obj)
        {
            this.psuRepo.CreateItem(obj);
        }

        /// <summary>
        /// Deletes entity by given id.
        /// </summary>
        /// <param name="id"> Id of the deletable entity. </param>
        public void DeleteItem(int id)
        {
            this.psuRepo.DeleteItem(id);
        }

        /// <summary>
        /// Returns a List of all PSU in the table.
        /// </summary>
        /// <returns> List of all PSU. </returns>
        public List<PSU> GetAll()
        {
            return this.psuRepo.GetAll();
        }

        /// <summary>
        /// Returns specified entity.
        /// </summary>
        /// <param name="id"> specified entity's number. </param>
        /// <returns> specified entity. </returns>
        public PSU GetOne(int id)
        {
            return this.psuRepo.GetOne(id);
        }

        /// <summary>
        /// Updates the specified entity's efficiency.
        /// </summary>
        /// <param name="id"> specified entity's number. </param>
        /// <param name="newValue"> new value of efficiency. </param>
        public void UpdateEfficiency(int id, int newValue)
        {
            this.psuRepo.UpdateEfficiency(id, newValue);
        }

        /// <summary>
        /// Updates the specified entity's name.
        /// </summary>
        /// <param name="id"> specified entity's number. </param>
        /// <param name="newValue"> new name. </param>
        public void UpdateName(int id, string newValue)
        {
            this.psuRepo.UpdateName(id, newValue);
        }

        /// <summary>
        /// Updates the specified entity's price.
        /// </summary>
        /// <param name="id"> specified entity's number. </param>
        /// <param name="newValue"> new price. </param>
        public void UpdatePrice(int id, int newValue)
        {
            this.psuRepo.UpdatePrice(id, newValue);
        }

        /// <summary>
        /// Averages the actual table all entity's price.
        /// </summary>
        /// <returns> Average price. </returns>
        public decimal AVGPrice()
        {
            decimal sumPrice = 0;
            foreach (var item in this.psuRepo.GetAll())
            {
                sumPrice += item.PRICE.Value;
            }

            sumPrice /= this.psuRepo.GetAll().Count;
            return sumPrice;
        }

        /// <summary>
        /// Looks for the last entity's ID.
        /// </summary>
        /// <returns> last entity's ID. </returns>
        public decimal GetMaxIdx()
        {
            return this.psuRepo.GetAll().Last().PSU_ID;
        }
    }

    /// <summary>
    /// Class of PC. Implements IPCLogic interface. Contains methods that relies on the same repository class as the controller one.
    /// </summary>
    public class PCLogic : IPCLogic
    {
        private readonly IPCRepository pCRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="PCLogic"/> class.
        /// Constructor which creates the database if none given.
        /// </summary>
        public PCLogic()
        {
            this.pCRepository = new PCRepository(new PCDBEntities());
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PCLogic"/> class.
        /// parameter gives the already created database to the class.
        /// </summary>
        /// <param name="ogRepo"> Pre-defined Database. </param>
        public PCLogic(IPCRepository ogRepo)
        {
            this.pCRepository = ogRepo;
        }

        /// <summary>
        /// Creates a new entity.
        /// </summary>
        /// <param name="obj"> a PC_SWITCHTABLE object. </param>
        public void AddPC(PC_SWITCHTABLE obj)
        {
            this.pCRepository.CreateItem(obj);
        }
    }
}
