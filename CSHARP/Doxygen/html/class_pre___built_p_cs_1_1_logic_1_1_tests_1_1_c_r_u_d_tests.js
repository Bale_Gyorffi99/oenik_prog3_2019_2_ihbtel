var class_pre___built_p_cs_1_1_logic_1_1_tests_1_1_c_r_u_d_tests =
[
    [ "Init", "class_pre___built_p_cs_1_1_logic_1_1_tests_1_1_c_r_u_d_tests.html#a2b08115908786ed489f0c2ac5f6cea32", null ],
    [ "Test_CreateNewPSU", "class_pre___built_p_cs_1_1_logic_1_1_tests_1_1_c_r_u_d_tests.html#a8bb97708cb4c907fbdadb469d6a0420b", null ],
    [ "Test_DeleteItemCPU", "class_pre___built_p_cs_1_1_logic_1_1_tests_1_1_c_r_u_d_tests.html#adee71286b8755cd17a82c51db5ac7b0b", null ],
    [ "Test_ModifyMBPrice", "class_pre___built_p_cs_1_1_logic_1_1_tests_1_1_c_r_u_d_tests.html#a6b43014473b3193f5c63568f92c7efda", null ],
    [ "Test_ReadAllRAM", "class_pre___built_p_cs_1_1_logic_1_1_tests_1_1_c_r_u_d_tests.html#a623b491b4c4f4330a81348ba373565ae", null ],
    [ "Test_ReadOneRAM", "class_pre___built_p_cs_1_1_logic_1_1_tests_1_1_c_r_u_d_tests.html#a723eac72c139b08a8d0c21bad0dcc050", null ]
];