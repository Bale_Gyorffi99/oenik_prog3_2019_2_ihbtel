var class_pre___built_p_cs_1_1_logic_1_1_m_b_logic =
[
    [ "MBLogic", "class_pre___built_p_cs_1_1_logic_1_1_m_b_logic.html#aefee46619ba44a0c0f1a15432c542759", null ],
    [ "MBLogic", "class_pre___built_p_cs_1_1_logic_1_1_m_b_logic.html#a7e9c4a41edd4346db67504451fd0c839", null ],
    [ "AVGPrice", "class_pre___built_p_cs_1_1_logic_1_1_m_b_logic.html#a5fe9d4ae8c80e7b17a9ac174de4005a8", null ],
    [ "CreateItem", "class_pre___built_p_cs_1_1_logic_1_1_m_b_logic.html#acbd7d9332b872419972211eae67434d2", null ],
    [ "DeleteItem", "class_pre___built_p_cs_1_1_logic_1_1_m_b_logic.html#abb2e65763f3f7b45c443161823fc36f9", null ],
    [ "FilterByForm", "class_pre___built_p_cs_1_1_logic_1_1_m_b_logic.html#a74140e2e2cec4ad5c25b614a929d62d0", null ],
    [ "GetAll", "class_pre___built_p_cs_1_1_logic_1_1_m_b_logic.html#a4fec06f8642794794a68ca5f79c6f7ce", null ],
    [ "GetMaxIdx", "class_pre___built_p_cs_1_1_logic_1_1_m_b_logic.html#a36e50e3f5482db904aaae1e4ebc28a1f", null ],
    [ "GetOne", "class_pre___built_p_cs_1_1_logic_1_1_m_b_logic.html#ad3ebdacde7a19f8e6a5cd098a80e3466", null ],
    [ "UpdateMemoryType", "class_pre___built_p_cs_1_1_logic_1_1_m_b_logic.html#a4ff0542e354510f0a2ecf8bf95d93fae", null ],
    [ "UpdateName", "class_pre___built_p_cs_1_1_logic_1_1_m_b_logic.html#a6e286d1a31b4da9e905fd9ee48ef45e5", null ],
    [ "UpdatePrice", "class_pre___built_p_cs_1_1_logic_1_1_m_b_logic.html#a649da23fbcd073cdc063c81022e84b01", null ],
    [ "UpdateVR", "class_pre___built_p_cs_1_1_logic_1_1_m_b_logic.html#aef7cf0813558fe7888fcfcddd2b773d2", null ]
];