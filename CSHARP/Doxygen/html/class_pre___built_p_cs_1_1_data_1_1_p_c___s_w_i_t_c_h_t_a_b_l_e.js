var class_pre___built_p_cs_1_1_data_1_1_p_c___s_w_i_t_c_h_t_a_b_l_e =
[
    [ "CPU", "class_pre___built_p_cs_1_1_data_1_1_p_c___s_w_i_t_c_h_t_a_b_l_e.html#a597c6512c68aa1fe13ee8a79f1077c9e", null ],
    [ "CPU_ID", "class_pre___built_p_cs_1_1_data_1_1_p_c___s_w_i_t_c_h_t_a_b_l_e.html#adde2956192edfd956c57abcafa4f90f4", null ],
    [ "GPU", "class_pre___built_p_cs_1_1_data_1_1_p_c___s_w_i_t_c_h_t_a_b_l_e.html#a63f102925fbdaef50fc654ba56ccad5c", null ],
    [ "GPU_ID", "class_pre___built_p_cs_1_1_data_1_1_p_c___s_w_i_t_c_h_t_a_b_l_e.html#a5d3b6b2fc5c2dfba5cc0b8e0eb8309a2", null ],
    [ "MD_ID", "class_pre___built_p_cs_1_1_data_1_1_p_c___s_w_i_t_c_h_t_a_b_l_e.html#adb8cbe3c048450ce24324553bcf8fb5f", null ],
    [ "MOTHERBOARD", "class_pre___built_p_cs_1_1_data_1_1_p_c___s_w_i_t_c_h_t_a_b_l_e.html#aac6cfddfbcef11e374599bc48884283b", null ],
    [ "PC_ID", "class_pre___built_p_cs_1_1_data_1_1_p_c___s_w_i_t_c_h_t_a_b_l_e.html#a687b79652b1ff4cbf27ea9c5072d3e17", null ],
    [ "PSU", "class_pre___built_p_cs_1_1_data_1_1_p_c___s_w_i_t_c_h_t_a_b_l_e.html#a689676b662dc50b3ec8e9cb208f16d21", null ],
    [ "PSU_ID", "class_pre___built_p_cs_1_1_data_1_1_p_c___s_w_i_t_c_h_t_a_b_l_e.html#ac9f9da1ea25093eaa8f696dcc958a56b", null ],
    [ "RAM", "class_pre___built_p_cs_1_1_data_1_1_p_c___s_w_i_t_c_h_t_a_b_l_e.html#a3719672a33fcce7bcf598f06e03ebcbd", null ],
    [ "RAM_AMOUNT", "class_pre___built_p_cs_1_1_data_1_1_p_c___s_w_i_t_c_h_t_a_b_l_e.html#aa4527915ec0aa90186bba5b331dd511f", null ],
    [ "RAM_ID", "class_pre___built_p_cs_1_1_data_1_1_p_c___s_w_i_t_c_h_t_a_b_l_e.html#afff44db3df528ec0d1256a894cff2cde", null ]
];