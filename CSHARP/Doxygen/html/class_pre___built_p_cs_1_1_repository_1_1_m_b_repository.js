var class_pre___built_p_cs_1_1_repository_1_1_m_b_repository =
[
    [ "MBRepository", "class_pre___built_p_cs_1_1_repository_1_1_m_b_repository.html#a71591f841d127988760b41eb99645fd7", null ],
    [ "CreateItem", "class_pre___built_p_cs_1_1_repository_1_1_m_b_repository.html#aad7e6826c5e06cebe9b8ffa57826c3bb", null ],
    [ "DeleteItem", "class_pre___built_p_cs_1_1_repository_1_1_m_b_repository.html#a3bef3449e212a7c0be9d40bc48b6c3f0", null ],
    [ "GetAll", "class_pre___built_p_cs_1_1_repository_1_1_m_b_repository.html#a343f6662ca2978fa9cd2f12fb1c836c8", null ],
    [ "GetOne", "class_pre___built_p_cs_1_1_repository_1_1_m_b_repository.html#ab04e5c8278cc91159f6d76b4ac741ebb", null ],
    [ "UpdateMemoryType", "class_pre___built_p_cs_1_1_repository_1_1_m_b_repository.html#a0d51db99a9143bf697a55a48cf7f988a", null ],
    [ "UpdateName", "class_pre___built_p_cs_1_1_repository_1_1_m_b_repository.html#a203098c104ad24cd5733e63807110054", null ],
    [ "UpdatePrice", "class_pre___built_p_cs_1_1_repository_1_1_m_b_repository.html#a13914a4916751732c83c3533a27abbe8", null ],
    [ "UpdateVR", "class_pre___built_p_cs_1_1_repository_1_1_m_b_repository.html#a6a467bcec97a79cf9f5a6eebcc0c07be", null ]
];