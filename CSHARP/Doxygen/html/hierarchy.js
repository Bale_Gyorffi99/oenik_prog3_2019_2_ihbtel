var hierarchy =
[
    [ "Pre_BuiltPCs.Logic.Tests.Class1", "class_pre___built_p_cs_1_1_logic_1_1_tests_1_1_class1.html", null ],
    [ "Pre_BuiltPCs.Data.CPU", "class_pre___built_p_cs_1_1_data_1_1_c_p_u.html", null ],
    [ "Pre_BuiltPCs.Logic.Tests.CRUDTests", "class_pre___built_p_cs_1_1_logic_1_1_tests_1_1_c_r_u_d_tests.html", null ],
    [ "DbContext", null, [
      [ "Pre_BuiltPCs.Data.PCDBEntities", "class_pre___built_p_cs_1_1_data_1_1_p_c_d_b_entities.html", null ]
    ] ],
    [ "Pre_BuiltPCs.Data.GPU", "class_pre___built_p_cs_1_1_data_1_1_g_p_u.html", null ],
    [ "Pre_BuiltPCs.Logic.ILogic< T >", "interface_pre___built_p_cs_1_1_logic_1_1_i_logic.html", null ],
    [ "Pre_BuiltPCs.Logic.ILogic< CPU >", "interface_pre___built_p_cs_1_1_logic_1_1_i_logic.html", [
      [ "Pre_BuiltPCs.Logic.ICPULogic", "interface_pre___built_p_cs_1_1_logic_1_1_i_c_p_u_logic.html", [
        [ "Pre_BuiltPCs.Logic.CPULogic", "class_pre___built_p_cs_1_1_logic_1_1_c_p_u_logic.html", null ]
      ] ]
    ] ],
    [ "Pre_BuiltPCs.Logic.ILogic< GPU >", "interface_pre___built_p_cs_1_1_logic_1_1_i_logic.html", [
      [ "Pre_BuiltPCs.Logic.IGPULogic", "interface_pre___built_p_cs_1_1_logic_1_1_i_g_p_u_logic.html", [
        [ "Pre_BuiltPCs.Logic.GPULogic", "class_pre___built_p_cs_1_1_logic_1_1_g_p_u_logic.html", null ]
      ] ]
    ] ],
    [ "Pre_BuiltPCs.Logic.ILogic< MOTHERBOARD >", "interface_pre___built_p_cs_1_1_logic_1_1_i_logic.html", [
      [ "Pre_BuiltPCs.Logic.IMBLogic", "interface_pre___built_p_cs_1_1_logic_1_1_i_m_b_logic.html", [
        [ "Pre_BuiltPCs.Logic.MBLogic", "class_pre___built_p_cs_1_1_logic_1_1_m_b_logic.html", null ]
      ] ]
    ] ],
    [ "Pre_BuiltPCs.Logic.ILogic< PSU >", "interface_pre___built_p_cs_1_1_logic_1_1_i_logic.html", [
      [ "Pre_BuiltPCs.Logic.IPSULogic", "interface_pre___built_p_cs_1_1_logic_1_1_i_p_s_u_logic.html", [
        [ "Pre_BuiltPCs.Logic.PSULogic", "class_pre___built_p_cs_1_1_logic_1_1_p_s_u_logic.html", null ]
      ] ]
    ] ],
    [ "Pre_BuiltPCs.Logic.ILogic< RAM >", "interface_pre___built_p_cs_1_1_logic_1_1_i_logic.html", [
      [ "Pre_BuiltPCs.Logic.IRAMLogic", "interface_pre___built_p_cs_1_1_logic_1_1_i_r_a_m_logic.html", [
        [ "Pre_BuiltPCs.Logic.RAMLogic", "class_pre___built_p_cs_1_1_logic_1_1_r_a_m_logic.html", null ]
      ] ]
    ] ],
    [ "Pre_BuiltPCs.Logic.IPCLogic", "interface_pre___built_p_cs_1_1_logic_1_1_i_p_c_logic.html", [
      [ "Pre_BuiltPCs.Logic.PCLogic", "class_pre___built_p_cs_1_1_logic_1_1_p_c_logic.html", null ]
    ] ],
    [ "Pre_BuiltPCs.Repository.IPCRepository", "interface_pre___built_p_cs_1_1_repository_1_1_i_p_c_repository.html", [
      [ "Pre_BuiltPCs.Repository.PCRepository", "class_pre___built_p_cs_1_1_repository_1_1_p_c_repository.html", null ]
    ] ],
    [ "Pre_BuiltPCs.Repository.IRepository< T >", "interface_pre___built_p_cs_1_1_repository_1_1_i_repository.html", null ],
    [ "Pre_BuiltPCs.Repository.IRepository< CPU >", "interface_pre___built_p_cs_1_1_repository_1_1_i_repository.html", [
      [ "Pre_BuiltPCs.Repository.ICPURepository", "interface_pre___built_p_cs_1_1_repository_1_1_i_c_p_u_repository.html", [
        [ "Pre_BuiltPCs.Repository.CPURepository", "class_pre___built_p_cs_1_1_repository_1_1_c_p_u_repository.html", null ]
      ] ]
    ] ],
    [ "Pre_BuiltPCs.Repository.IRepository< GPU >", "interface_pre___built_p_cs_1_1_repository_1_1_i_repository.html", [
      [ "Pre_BuiltPCs.Repository.IGPURepository", "interface_pre___built_p_cs_1_1_repository_1_1_i_g_p_u_repository.html", [
        [ "Pre_BuiltPCs.Repository.GPURepository", "class_pre___built_p_cs_1_1_repository_1_1_g_p_u_repository.html", null ]
      ] ]
    ] ],
    [ "Pre_BuiltPCs.Repository.IRepository< MOTHERBOARD >", "interface_pre___built_p_cs_1_1_repository_1_1_i_repository.html", [
      [ "Pre_BuiltPCs.Repository.IMBRepository", "interface_pre___built_p_cs_1_1_repository_1_1_i_m_b_repository.html", [
        [ "Pre_BuiltPCs.Repository.MBRepository", "class_pre___built_p_cs_1_1_repository_1_1_m_b_repository.html", null ]
      ] ]
    ] ],
    [ "Pre_BuiltPCs.Repository.IRepository< PSU >", "interface_pre___built_p_cs_1_1_repository_1_1_i_repository.html", [
      [ "Pre_BuiltPCs.Repository.IPSURepository", "interface_pre___built_p_cs_1_1_repository_1_1_i_p_s_u_repository.html", [
        [ "Pre_BuiltPCs.Repository.PSURepository", "class_pre___built_p_cs_1_1_repository_1_1_p_s_u_repository.html", null ]
      ] ]
    ] ],
    [ "Pre_BuiltPCs.Repository.IRepository< RAM >", "interface_pre___built_p_cs_1_1_repository_1_1_i_repository.html", [
      [ "Pre_BuiltPCs.Repository.IRAMRepository", "interface_pre___built_p_cs_1_1_repository_1_1_i_r_a_m_repository.html", [
        [ "Pre_BuiltPCs.Repository.RAMRepository", "class_pre___built_p_cs_1_1_repository_1_1_r_a_m_repository.html", null ]
      ] ]
    ] ],
    [ "Pre_BuiltPCs.Data.MOTHERBOARD", "class_pre___built_p_cs_1_1_data_1_1_m_o_t_h_e_r_b_o_a_r_d.html", null ],
    [ "Pre_BuiltPCs.Logic.Tests.NonCRUDTests", "class_pre___built_p_cs_1_1_logic_1_1_tests_1_1_non_c_r_u_d_tests.html", null ],
    [ "Pre_BuiltPCs.Data.PC_SWITCHTABLE", "class_pre___built_p_cs_1_1_data_1_1_p_c___s_w_i_t_c_h_t_a_b_l_e.html", null ],
    [ "Pre_BuiltPCs.Program.Program", "class_pre___built_p_cs_1_1_program_1_1_program.html", null ],
    [ "Pre_BuiltPCs.Data.PSU", "class_pre___built_p_cs_1_1_data_1_1_p_s_u.html", null ],
    [ "Pre_BuiltPCs.Data.RAM", "class_pre___built_p_cs_1_1_data_1_1_r_a_m.html", null ]
];