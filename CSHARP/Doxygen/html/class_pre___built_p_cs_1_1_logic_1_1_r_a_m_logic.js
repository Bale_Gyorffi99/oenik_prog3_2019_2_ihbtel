var class_pre___built_p_cs_1_1_logic_1_1_r_a_m_logic =
[
    [ "RAMLogic", "class_pre___built_p_cs_1_1_logic_1_1_r_a_m_logic.html#a9ee91773ecef84055fe6bd73e9d98cc1", null ],
    [ "RAMLogic", "class_pre___built_p_cs_1_1_logic_1_1_r_a_m_logic.html#a377f94a39a70f25900d16ac008227145", null ],
    [ "AVGPrice", "class_pre___built_p_cs_1_1_logic_1_1_r_a_m_logic.html#ad92731472d940a7ebad8107ba01cf55e", null ],
    [ "CreateItem", "class_pre___built_p_cs_1_1_logic_1_1_r_a_m_logic.html#aa6e36ce6371e7bf3ab74f04624f513f7", null ],
    [ "DeleteItem", "class_pre___built_p_cs_1_1_logic_1_1_r_a_m_logic.html#a3818883a1d364711e067fd7444acdb76", null ],
    [ "FilterBySpeed", "class_pre___built_p_cs_1_1_logic_1_1_r_a_m_logic.html#a4373878fc7a42db6cfdc972fe8ef8a77", null ],
    [ "GetAll", "class_pre___built_p_cs_1_1_logic_1_1_r_a_m_logic.html#a31632cb621fd165cd173710df491f898", null ],
    [ "GetMaxIdx", "class_pre___built_p_cs_1_1_logic_1_1_r_a_m_logic.html#a7c880c890417ddb85e1ab36521a39149", null ],
    [ "GetOne", "class_pre___built_p_cs_1_1_logic_1_1_r_a_m_logic.html#a8df39e2f20a5c7100d4f65e9f7c406db", null ],
    [ "UpdateName", "class_pre___built_p_cs_1_1_logic_1_1_r_a_m_logic.html#ae94f032af9149403563b7f5069d5df0e", null ],
    [ "UpdatePrice", "class_pre___built_p_cs_1_1_logic_1_1_r_a_m_logic.html#ad668aff7b5219d8d1b26e29b1e371733", null ],
    [ "UpdateRAMType", "class_pre___built_p_cs_1_1_logic_1_1_r_a_m_logic.html#ab3e8513b2d457a356ac54ab8e69f20c4", null ]
];