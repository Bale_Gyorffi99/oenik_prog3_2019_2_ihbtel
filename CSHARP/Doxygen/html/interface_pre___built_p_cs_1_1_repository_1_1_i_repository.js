var interface_pre___built_p_cs_1_1_repository_1_1_i_repository =
[
    [ "CreateItem", "interface_pre___built_p_cs_1_1_repository_1_1_i_repository.html#af2788c8210373610606fa00503f617ca", null ],
    [ "DeleteItem", "interface_pre___built_p_cs_1_1_repository_1_1_i_repository.html#add957d58f47d112d07839ab9380b5359", null ],
    [ "GetAll", "interface_pre___built_p_cs_1_1_repository_1_1_i_repository.html#a796e410186c7bb56ba0a4830025f8b0b", null ],
    [ "GetOne", "interface_pre___built_p_cs_1_1_repository_1_1_i_repository.html#a14a861855e9f863a0f24d63acfc78afc", null ],
    [ "UpdateName", "interface_pre___built_p_cs_1_1_repository_1_1_i_repository.html#a68df01e26b385755d1632868ae399737", null ],
    [ "UpdatePrice", "interface_pre___built_p_cs_1_1_repository_1_1_i_repository.html#a417f604d49990ec137d7976112f6bc45", null ]
];