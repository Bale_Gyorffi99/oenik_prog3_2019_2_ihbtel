var class_pre___built_p_cs_1_1_logic_1_1_g_p_u_logic =
[
    [ "GPULogic", "class_pre___built_p_cs_1_1_logic_1_1_g_p_u_logic.html#a45043d803dfb490bd62bb2920594d375", null ],
    [ "GPULogic", "class_pre___built_p_cs_1_1_logic_1_1_g_p_u_logic.html#a17073a406fc4e750d480a6eeec42bc6e", null ],
    [ "AVGPrice", "class_pre___built_p_cs_1_1_logic_1_1_g_p_u_logic.html#a8d3697af1f75c3398527834a1a0133ae", null ],
    [ "CreateItem", "class_pre___built_p_cs_1_1_logic_1_1_g_p_u_logic.html#a096e7aceaef787ee4f65381950be0c30", null ],
    [ "DeleteItem", "class_pre___built_p_cs_1_1_logic_1_1_g_p_u_logic.html#a8710c2bee0bd11c5189524072ae6f7ca", null ],
    [ "FilterPriceBetween", "class_pre___built_p_cs_1_1_logic_1_1_g_p_u_logic.html#a405efa675cb534801eba68fa372c5da6", null ],
    [ "GetAll", "class_pre___built_p_cs_1_1_logic_1_1_g_p_u_logic.html#ac65a2719a66b02597fd7e60290a7a1c2", null ],
    [ "GetMaxIdx", "class_pre___built_p_cs_1_1_logic_1_1_g_p_u_logic.html#a92ebea3c3fdf2dd828e5ecfff2b36922", null ],
    [ "GetOne", "class_pre___built_p_cs_1_1_logic_1_1_g_p_u_logic.html#ab91368957f4f8dd353a261d970fe9512", null ],
    [ "UpdateName", "class_pre___built_p_cs_1_1_logic_1_1_g_p_u_logic.html#ad4a77f5ca758943d376ef34a65ac71f3", null ],
    [ "UpdatePrice", "class_pre___built_p_cs_1_1_logic_1_1_g_p_u_logic.html#ac8b2a84c92ae58629d85d14aa24a19fb", null ],
    [ "UpdateVR", "class_pre___built_p_cs_1_1_logic_1_1_g_p_u_logic.html#af56863a52d6a3069609bebc85452c9c4", null ]
];