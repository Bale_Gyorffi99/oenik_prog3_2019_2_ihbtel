var namespace_pre___built_p_cs_1_1_data =
[
    [ "CPU", "class_pre___built_p_cs_1_1_data_1_1_c_p_u.html", "class_pre___built_p_cs_1_1_data_1_1_c_p_u" ],
    [ "GPU", "class_pre___built_p_cs_1_1_data_1_1_g_p_u.html", "class_pre___built_p_cs_1_1_data_1_1_g_p_u" ],
    [ "MOTHERBOARD", "class_pre___built_p_cs_1_1_data_1_1_m_o_t_h_e_r_b_o_a_r_d.html", "class_pre___built_p_cs_1_1_data_1_1_m_o_t_h_e_r_b_o_a_r_d" ],
    [ "PC_SWITCHTABLE", "class_pre___built_p_cs_1_1_data_1_1_p_c___s_w_i_t_c_h_t_a_b_l_e.html", "class_pre___built_p_cs_1_1_data_1_1_p_c___s_w_i_t_c_h_t_a_b_l_e" ],
    [ "PCDBEntities", "class_pre___built_p_cs_1_1_data_1_1_p_c_d_b_entities.html", "class_pre___built_p_cs_1_1_data_1_1_p_c_d_b_entities" ],
    [ "PSU", "class_pre___built_p_cs_1_1_data_1_1_p_s_u.html", "class_pre___built_p_cs_1_1_data_1_1_p_s_u" ],
    [ "RAM", "class_pre___built_p_cs_1_1_data_1_1_r_a_m.html", "class_pre___built_p_cs_1_1_data_1_1_r_a_m" ]
];