var interface_pre___built_p_cs_1_1_logic_1_1_i_logic =
[
    [ "CreateItem", "interface_pre___built_p_cs_1_1_logic_1_1_i_logic.html#a1c5d5a212a1c9c42aa8450ceabfaa599", null ],
    [ "DeleteItem", "interface_pre___built_p_cs_1_1_logic_1_1_i_logic.html#a9357ef56df1431c54b801b29da9c25c6", null ],
    [ "GetAll", "interface_pre___built_p_cs_1_1_logic_1_1_i_logic.html#a9ce2e7b6c694b91c019eda4f33243065", null ],
    [ "GetMaxIdx", "interface_pre___built_p_cs_1_1_logic_1_1_i_logic.html#a163b2e74244388ed4e1e9c5c34042b44", null ],
    [ "GetOne", "interface_pre___built_p_cs_1_1_logic_1_1_i_logic.html#abd44f845ac34841b4a110e849582db04", null ],
    [ "UpdateName", "interface_pre___built_p_cs_1_1_logic_1_1_i_logic.html#a06a9f1ed4ca28ecbe0aec6dd82f56417", null ],
    [ "UpdatePrice", "interface_pre___built_p_cs_1_1_logic_1_1_i_logic.html#a34f44cecaf0aabcd00d6a6f84d9a1067", null ]
];