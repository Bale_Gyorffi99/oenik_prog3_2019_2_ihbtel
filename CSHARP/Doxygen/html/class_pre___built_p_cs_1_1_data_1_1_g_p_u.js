var class_pre___built_p_cs_1_1_data_1_1_g_p_u =
[
    [ "GPU", "class_pre___built_p_cs_1_1_data_1_1_g_p_u.html#a24b9c645a9be293acb4f9d835a723d6a", null ],
    [ "CLOCK_SPEED", "class_pre___built_p_cs_1_1_data_1_1_g_p_u.html#a8b218553b324745c633bbf4b56c40e04", null ],
    [ "GPU_ID", "class_pre___built_p_cs_1_1_data_1_1_g_p_u.html#aaa5c0f41ed5dea2044429fc28f6f9ed6", null ],
    [ "GPU_NAME", "class_pre___built_p_cs_1_1_data_1_1_g_p_u.html#aec9077ae16b2975c1ac26ebd82dcc365", null ],
    [ "PC_SWITCHTABLE", "class_pre___built_p_cs_1_1_data_1_1_g_p_u.html#ab7cd3caa915b118dbd8710258b3f9675", null ],
    [ "PRICE", "class_pre___built_p_cs_1_1_data_1_1_g_p_u.html#a1016247979085e165d84c0a3ff2cd55f", null ],
    [ "RAM_SIZE", "class_pre___built_p_cs_1_1_data_1_1_g_p_u.html#ab220302f0ad16274ee3dbd4bb3cc78df", null ],
    [ "SERIALNUMBER", "class_pre___built_p_cs_1_1_data_1_1_g_p_u.html#ab5fdce36b821ad9958f3486f687bd432", null ],
    [ "VR_READY", "class_pre___built_p_cs_1_1_data_1_1_g_p_u.html#a7f40de538455f6927d0b3c196dc8c95b", null ]
];