var class_pre___built_p_cs_1_1_data_1_1_r_a_m =
[
    [ "RAM", "class_pre___built_p_cs_1_1_data_1_1_r_a_m.html#a447b7a4980ba325eb6123522cde7e9da", null ],
    [ "CL", "class_pre___built_p_cs_1_1_data_1_1_r_a_m.html#a6f00d9afcbb70e51ee9a1c39472e3917", null ],
    [ "PC_SWITCHTABLE", "class_pre___built_p_cs_1_1_data_1_1_r_a_m.html#ad5214bf2c173ea2db37ecef83fb5af1b", null ],
    [ "PRICE", "class_pre___built_p_cs_1_1_data_1_1_r_a_m.html#aca9bd9891e54d4cb3e76470e9cb60230", null ],
    [ "RAM_ID", "class_pre___built_p_cs_1_1_data_1_1_r_a_m.html#a6795856c7a4bfaf7239f8de5323c4064", null ],
    [ "RAM_NAME", "class_pre___built_p_cs_1_1_data_1_1_r_a_m.html#a2d3dc34c9e1d8511e9fe0ad17efab0cb", null ],
    [ "RAM_SIZE", "class_pre___built_p_cs_1_1_data_1_1_r_a_m.html#aa891744f2610db6bb20b25ec74a4ed5a", null ],
    [ "RAM_TYPE", "class_pre___built_p_cs_1_1_data_1_1_r_a_m.html#aa70eefb251da1b5e39da1dc74033636d", null ],
    [ "SERIALNUMBER", "class_pre___built_p_cs_1_1_data_1_1_r_a_m.html#abc381098bc027bbf42c2111919c538b9", null ],
    [ "SPEED", "class_pre___built_p_cs_1_1_data_1_1_r_a_m.html#a36240d5454a0b8fdafba73b9908ea661", null ]
];