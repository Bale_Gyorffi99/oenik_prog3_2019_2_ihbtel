var class_pre___built_p_cs_1_1_data_1_1_m_o_t_h_e_r_b_o_a_r_d =
[
    [ "MOTHERBOARD", "class_pre___built_p_cs_1_1_data_1_1_m_o_t_h_e_r_b_o_a_r_d.html#a6e06f303483a31d32801d6b319f8e3b7", null ],
    [ "FORM_FACTOR", "class_pre___built_p_cs_1_1_data_1_1_m_o_t_h_e_r_b_o_a_r_d.html#ae302cd892d8065a23ae9795b207478a2", null ],
    [ "MB_NAME", "class_pre___built_p_cs_1_1_data_1_1_m_o_t_h_e_r_b_o_a_r_d.html#a4d1ac148c973392bb4142ec66e471383", null ],
    [ "MD_ID", "class_pre___built_p_cs_1_1_data_1_1_m_o_t_h_e_r_b_o_a_r_d.html#a6e48d0723ef5928166a1a7ded2c6f78a", null ],
    [ "MEMORY_SLOTS", "class_pre___built_p_cs_1_1_data_1_1_m_o_t_h_e_r_b_o_a_r_d.html#a2bb57d482f68a73b1cc901f6cf392907", null ],
    [ "MEMORY_TYPE", "class_pre___built_p_cs_1_1_data_1_1_m_o_t_h_e_r_b_o_a_r_d.html#aa62e0f378002497cee759c87c1ad97c3", null ],
    [ "PC_SWITCHTABLE", "class_pre___built_p_cs_1_1_data_1_1_m_o_t_h_e_r_b_o_a_r_d.html#acd8a838faaaa1b62be6c60a4ffa61e2f", null ],
    [ "PRICE", "class_pre___built_p_cs_1_1_data_1_1_m_o_t_h_e_r_b_o_a_r_d.html#a4dbd740b43d6bbe9ff73e6080cef6036", null ],
    [ "SERIALNUMBER", "class_pre___built_p_cs_1_1_data_1_1_m_o_t_h_e_r_b_o_a_r_d.html#a305646554e1af24dacb0972224ef6dd1", null ]
];