var class_pre___built_p_cs_1_1_repository_1_1_r_a_m_repository =
[
    [ "RAMRepository", "class_pre___built_p_cs_1_1_repository_1_1_r_a_m_repository.html#a1a9ff95e6e9dfa983e1d05277d1fb1b2", null ],
    [ "CreateItem", "class_pre___built_p_cs_1_1_repository_1_1_r_a_m_repository.html#afdee3c077d499b691523151a90050385", null ],
    [ "DeleteItem", "class_pre___built_p_cs_1_1_repository_1_1_r_a_m_repository.html#af76005299556ec665ad21f4da87c1c1b", null ],
    [ "GetAll", "class_pre___built_p_cs_1_1_repository_1_1_r_a_m_repository.html#a0f71e4863ed39c9ddecc6aff71f8fde7", null ],
    [ "GetOne", "class_pre___built_p_cs_1_1_repository_1_1_r_a_m_repository.html#afc73d4e407742bf83b3aaca0c0be427b", null ],
    [ "UpdateName", "class_pre___built_p_cs_1_1_repository_1_1_r_a_m_repository.html#abc04fe67f54a700b0a661d7ca749cb34", null ],
    [ "UpdatePrice", "class_pre___built_p_cs_1_1_repository_1_1_r_a_m_repository.html#a91c4177478def52ddfa89ea5eb3ee6a3", null ],
    [ "UpdateRAMType", "class_pre___built_p_cs_1_1_repository_1_1_r_a_m_repository.html#ace92ffdba571fbd1c86f51f2ab59c59f", null ]
];