var class_pre___built_p_cs_1_1_data_1_1_c_p_u =
[
    [ "CPU", "class_pre___built_p_cs_1_1_data_1_1_c_p_u.html#a0b5c6e3945f88eb89b2cc7f6afc00364", null ],
    [ "CORES", "class_pre___built_p_cs_1_1_data_1_1_c_p_u.html#a2b1610e16ac2e7f8c06d46f7e45dbc42", null ],
    [ "CPU_ID", "class_pre___built_p_cs_1_1_data_1_1_c_p_u.html#ac366142bf5d08d8c5e94310f3423a882", null ],
    [ "CPU_NAME", "class_pre___built_p_cs_1_1_data_1_1_c_p_u.html#aa1530e5afb861ed99b7acf32b609f37d", null ],
    [ "FREQUENCY", "class_pre___built_p_cs_1_1_data_1_1_c_p_u.html#ae5b27e1f7ea1ff28d549b7ab4c3bbc3b", null ],
    [ "PC_SWITCHTABLE", "class_pre___built_p_cs_1_1_data_1_1_c_p_u.html#afba41b0d7fbeb67d53c02b95514aa2c9", null ],
    [ "PRICE", "class_pre___built_p_cs_1_1_data_1_1_c_p_u.html#a849836a3ef9d0d0f73575c5104f90b9e", null ],
    [ "SERIALNUMBER", "class_pre___built_p_cs_1_1_data_1_1_c_p_u.html#af9e43869254abffb49345fed3251b090", null ],
    [ "SOCKET", "class_pre___built_p_cs_1_1_data_1_1_c_p_u.html#ab1487de370d95118c537c687bab80c92", null ],
    [ "THREADS", "class_pre___built_p_cs_1_1_data_1_1_c_p_u.html#ab35d165534d01a44ad01ffe48a995be7", null ]
];