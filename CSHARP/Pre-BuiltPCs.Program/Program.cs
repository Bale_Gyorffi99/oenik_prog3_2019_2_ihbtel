﻿// <copyright file="Program.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Pre_BuiltPCs.Program
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Xml.Linq;
    using Pre_BuiltPCs.Data;
    using Pre_BuiltPCs.Logic;

/*
                                                      ____                 ____        _ _ _     ____   ____
                                                     |  _ \ _ __ ___      | __ ) _   _(_) | |_  |  _ \ / ___|
                                                     | |_) | '__/ _ \_____|  _ \| | | | | | __| | |_) | |
                                                     |  __/| | |  __/_____| |_) | |_| | | | |_  |  __/| |___
                                                     |_|   |_|  \___|     |____/ \__,_|_|_|\__| |_|    \____|
*/
    /// <summary>
    /// Contains the main program and all the method calling.
    /// </summary>
    internal class Program
    {
        private static readonly Random R = new Random();

        private static void Main(string[] args)
        {
            Console.WriteLine("Welcome in this workshop. What would you like to do?");
            Console.WriteLine("Press 1 to: list Entities");
            Console.WriteLine("Press 2 to: Add item");
            Console.WriteLine("Press 3 to: Update existing item");
            Console.WriteLine("Press 4 to: Delete existing item");
            Console.WriteLine("Press 5 to: Avg price of itemgroup");
            Console.WriteLine("Press 6 to: Filter by preset filters");
            Console.WriteLine("press 7 to: Build own pc");
            Console.WriteLine("press 8 to: Randomize a pc");
            Console.Write("Your choice is: ");
            ChooseMenu(Console.ReadKey().KeyChar);
        }

        private static void ChooseMenu(char c)
        {
            try
            {
                switch (c)
                {
                    case '1':
                        ReadEntities();
                        break;
                    case '2':
                        CreateEntity();
                        Console.WriteLine("Creation completed!");
                        break;
                    case '3':
                        UpdateEntity();
                        Console.WriteLine("Modify completed!");
                        break;
                    case '4':
                        DeleteEntity();
                        Console.WriteLine("Delete completed!");
                        break;
                    case '5':
                        AVGPrice();
                        break;
                    case '6':
                        Filtering();
                        break;
                    case '7':
                        BuildPC();
                        break;
                    case '8':
                        JavaBuildPC();
                        break;
                    default:
                        throw new InvalidCastException();
                }
            }
            catch (InvalidCastException)
            {
                Console.WriteLine("\nYou can choose only from 1 - 8.\nTry Again.");
                ChooseMenu(Console.ReadKey().KeyChar);
            }
        }

        private static void CreateEntity()
        {
            MBLogic mbLog = new MBLogic();
            RAMLogic ramLog = new RAMLogic();
            PSULogic psuLog = new PSULogic();
            CPULogic cpuLog = new CPULogic();
            GPULogic gpuLog = new GPULogic();

            Console.WriteLine("\n\nSelect a table to create a new entity:");
            Console.WriteLine("Press 1 to create a RAM");
            Console.WriteLine("Press 2 to create a Motherboard");
            Console.WriteLine("Press 3 to create a CPU");
            Console.WriteLine("Press 4 to create a GPU");
            Console.WriteLine("Press 5 to create a PSU");
            char c = Console.ReadKey().KeyChar;

            Console.WriteLine("What should be its name?");
            string newEntitiyName = Console.ReadLine();

            Console.WriteLine("How much does it cost?");
            int newEntityCost = int.Parse(Console.ReadLine());
            int serialNumber = R.Next(111111111, 999999999);
            int idNumber = R.Next(1121, 9999);
            switch (c)
            {
                case '1':
                    Console.WriteLine("How much ram should it have? eg.: 8, 16, 4..");
                    int ramSize = int.Parse(Console.ReadLine());
                    Console.WriteLine("What should be the type of the ram? eg.: DDR4, DDR3, DDR5..");
                    string ramType = Console.ReadLine();
                    Console.WriteLine("Memory speed? eg.: 2666, 3000, 3200..");
                    int ramSpeed = int.Parse(Console.ReadLine());
                    Console.WriteLine("CL? eg.: 18, 16, 14...");
                    int ramCL = int.Parse(Console.ReadLine());
                    ramLog.CreateItem(new RAM()
                    {
                        RAM_ID = idNumber, // TODO: Maxkiválasztás
                        RAM_NAME = newEntitiyName,
                        SERIALNUMBER = serialNumber,
                        RAM_SIZE = ramSize,
                        RAM_TYPE = ramType,
                        SPEED = ramSpeed,
                        PRICE = newEntityCost,
                    });
                    break;
                case '2':
                    Console.WriteLine("Should it be ATX, M-ATX, or E-ATX?");
                    string formFactor = Console.ReadLine();
                    Console.WriteLine("What memory should it support? eg.: DDR4, DDR5..");
                    string mbMemoryType = Console.ReadLine();
                    Console.WriteLine("How many slots should it have? eg.: 2, 4, 8..");
                    int mbMemorySlot = int.Parse(Console.ReadLine());
                    mbLog.CreateItem(new MOTHERBOARD()
                    {
                        MB_NAME = newEntitiyName,
                        MD_ID = idNumber,
                        SERIALNUMBER = serialNumber,
                        PRICE = newEntityCost,
                        FORM_FACTOR = formFactor,
                        MEMORY_TYPE = mbMemoryType,
                        MEMORY_SLOTS = mbMemorySlot,
                    });
                    break;
                case '3':
                    Console.WriteLine("How many cores should it have? eg.: 2, 4, 12..");
                    int cpuCores = int.Parse(Console.ReadLine());
                    Console.WriteLine("Should it be an Intel or an AMD processor?");
                    string cpuSocket = Console.ReadLine();
                    Console.WriteLine("What should be its frequency? eg.: 3200, 2600, 4100..");
                    int cpuFrequency = int.Parse(Console.ReadLine());
                    cpuLog.CreateItem(new CPU()
                    {
                        CPU_ID = idNumber,
                        PRICE = newEntityCost,
                        CPU_NAME = newEntitiyName,
                        CORES = cpuCores,
                        SERIALNUMBER = serialNumber,
                        SOCKET = cpuSocket,
                        FREQUENCY = cpuFrequency,
                        THREADS = cpuCores * 2,
                    });
                    break;
                case '4':
                    Console.WriteLine("What should be its ram size? eg.: 2, 4, 8..");
                    int gpuRamSize = int.Parse(Console.ReadLine());
                    Console.WriteLine("What should be its boost clock speed? eg.: 1625, 1800, 1910..");
                    int gpuClockSpeed = int.Parse(Console.ReadLine());
                    Console.WriteLine("Should it be VR ready? yes or no");
                    string vr_ready = Console.ReadLine();
                    int gpuVR;
                    if (vr_ready == "yes")
                    {
                        gpuVR = 1;
                    }
                    else
                    {
                        gpuVR = 0;
                    }

                    gpuLog.CreateItem(new GPU()
                    {
                        GPU_ID = idNumber,
                        PRICE = newEntityCost,
                        GPU_NAME = newEntitiyName,
                        SERIALNUMBER = serialNumber,
                        RAM_SIZE = gpuRamSize,
                        CLOCK_SPEED = gpuClockSpeed,
                        VR_READY = gpuVR,
                    });
                    break;
                case '5':
                    Console.WriteLine("How much watt should it hold? eg.: 400, 600, 900..");
                    int psuWattage = int.Parse(Console.ReadLine());
                    Console.WriteLine("How efficient should it be? eg.: 82, 88, 92..");
                    int psuEfficiency = int.Parse(Console.ReadLine());
                    Console.WriteLine("How much should be its weight? eg.: 1.7, 2.0, 2.5..");
                    decimal psuWeight = decimal.Parse(Console.ReadLine());
                    psuLog.CreateItem(new PSU()
                    {
                        PSU_ID = idNumber,
                        PSU_NAME = newEntitiyName,
                        PRICE = newEntityCost,
                        SERIALNUMBER = serialNumber,
                        WATTAGE = psuWattage,
                        PSU_WEIGHT = psuWeight,
                        EFFICIENCY = psuEfficiency,
                    });
                    break;
                default:
                    break;
            }
        }

        private static void ReadEntities()
        {
            Console.WriteLine("\n\nSelect a table to list the content:");
            Console.WriteLine("Press 1 to list RAMs");
            Console.WriteLine("Press 2 to list Motherboards");
            Console.WriteLine("Press 3 to list CPUs");
            Console.WriteLine("Press 4 to list GPUs");
            Console.WriteLine("Press 5 to list PSUs");
            char c = Console.ReadKey().KeyChar;
            try
            {
                switch (c)
                {
                    case '1':
                        RAMLogic ramLogic = new RAMLogic();
                        foreach (var item in ramLogic.GetAll())
                        {
                            Console.WriteLine("\nID: {0} - NAME: {1} - PRICE: {2} HUF - SPEED: {3}", item.RAM_ID, item.RAM_NAME, item.PRICE, item.SPEED);
                        }

                        break;
                    case '2':
                        MBLogic mbLogic = new MBLogic();
                        foreach (var item in mbLogic.GetAll())
                        {
                            Console.WriteLine("\nID: {0} - NAME: {1} - PRICE: {2} HUF - FORM: {3}", item.MD_ID, item.MB_NAME, item.PRICE, item.FORM_FACTOR);
                        }

                        break;
                    case '3':
                        CPULogic cpuLogic = new CPULogic();
                        foreach (var item in cpuLogic.GetAll())
                        {
                            Console.WriteLine("\nID: {0} - NAME: {1} - PRICE: {2} HUF - CORES: {3}", item.CPU_ID, item.CPU_NAME, item.PRICE, item.CORES);
                        }

                        break;
                    case '4':
                        GPULogic gpulogic = new GPULogic();
                        foreach (var item in gpulogic.GetAll())
                        {
                            Console.WriteLine("\nID: {0} - NAME: {1} - PRICE: {2} HUF - MEMORY: {3}", item.GPU_ID, item.GPU_NAME, item.PRICE, item.RAM_SIZE);
                        }

                        break;
                    case '5':
                        PSULogic psuLogic = new PSULogic();
                        foreach (var item in psuLogic.GetAll())
                        {
                            Console.WriteLine("\nID: {0} - NAME: {1} - PRICE: {2} HUF - WATTAGE: {3}", item.PSU_ID, item.PSU_NAME, item.PRICE, item.WATTAGE);
                        }

                        break;
                    default:
                        throw new InvalidCastException();
                }
            }
            catch (InvalidCastException)
            {
                Console.WriteLine("\nYou can choose only from 1 - 5.\nTry Again.");
                ReadEntities();
            }
        }

        private static void UpdateEntity()
        {
            Console.WriteLine("\n\nSelect a table to update the content:");
            Console.WriteLine("Press 1 to update one of the RAMs");
            Console.WriteLine("Press 2 to update one of the Motherboards");
            Console.WriteLine("Press 3 to update one of the CPUs");
            Console.WriteLine("Press 4 to update one of the GPUs");
            Console.WriteLine("Press 5 to update one of the PSUs");
            char tableIndex = Console.ReadKey().KeyChar;
            try
            {
                int i = 0;
                switch (tableIndex)
                {
                    case '1':
                        RAMLogic ramLogic = new RAMLogic();
                        foreach (var item in ramLogic.GetAll())
                        {
                            Console.WriteLine("\n[{4}] ID: {0} - NAME: {1} - PRICE: {2} HUF - SPEED: {3}", item.RAM_ID, item.RAM_NAME, item.PRICE, item.SPEED, i);
                            i++;
                        }

                        Console.WriteLine("Which ram would you like to update? (use ID to Identify)");
                        int ramIndex = int.Parse(Console.ReadLine());
                        if (!ramLogic.GetOne(ramIndex).Equals(null))
                        {
                            Console.WriteLine("Would you like to modify its name, price or type?");
                            string ramModifier;
                            do
                            {
                                Console.Write("Choice: ");
                                ramModifier = Console.ReadLine();
                            }
                            while (ramModifier != "name" && ramModifier != "price" && ramModifier != "type");
                            switch (ramModifier)
                            {
                                case "name":
                                    Console.Write("What should be its new name?: ");
                                    string ramNewName = Console.ReadLine();
                                    ramLogic.UpdateName(ramIndex, ramNewName);
                                    break;
                                case "price":
                                    Console.Write("What should be its new price?: ");
                                    string ramNewPrice = Console.ReadLine();
                                    ramLogic.UpdatePrice(ramIndex, int.Parse(ramNewPrice));
                                    break;
                                case "type":
                                    Console.Write("What should be its new type?: ");
                                    string ramNewType = Console.ReadLine();
                                    ramLogic.UpdateRAMType(ramIndex, ramNewType);
                                    break;
                                default:
                                    break;
                            }
                        }
                        else
                        {
                            Console.WriteLine("You chose a wrong entity. Try again.");
                            UpdateEntity();
                        }

                        break;
                    case '2':
                        MBLogic mbLogic = new MBLogic();
                        foreach (var item in mbLogic.GetAll())
                        {
                            Console.WriteLine("\n[{4}] ID: {0} - NAME: {1} - PRICE: {2} HUF - FORM: {3}", item.MD_ID, item.MB_NAME, item.PRICE, item.FORM_FACTOR, i);
                            i++;
                        }

                        Console.WriteLine("Which motherboard would you like to update? (use ID to Identify)");
                        int mbIndex = int.Parse(Console.ReadLine());
                        if (!mbLogic.GetOne(mbIndex).Equals(null))
                        {
                            Console.WriteLine("Would you like to modify its name, price or memory type?");
                            string mbModifier;
                            do
                            {
                                Console.Write("Choice: ");
                                mbModifier = Console.ReadLine();
                            }
                            while (mbModifier != "name" && mbModifier != "price" && mbModifier != "memory type");
                            switch (mbModifier)
                            {
                                case "name":
                                    Console.Write("What should be its new name?: ");
                                    string mbNewName = Console.ReadLine();
                                    mbLogic.UpdateName(mbIndex, mbNewName);
                                    break;
                                case "price":
                                    Console.Write("What should be its new price?: ");
                                    string mbNewPrice = Console.ReadLine();
                                    mbLogic.UpdatePrice(mbIndex, int.Parse(mbNewPrice));
                                    break;
                                case "type":
                                    Console.Write("What should be its new memory type?: ");
                                    string mbNewType = Console.ReadLine();
                                    mbLogic.UpdateMemoryType(mbIndex, mbNewType);
                                    break;
                                default:
                                    break;
                            }
                        }
                        else
                        {
                            Console.WriteLine("You chose a wrong entity. Try again.");
                            UpdateEntity();
                        }

                        break;
                    case '3':
                        CPULogic cpuLogic = new CPULogic();
                        foreach (var item in cpuLogic.GetAll())
                        {
                            Console.WriteLine("\n[{4}] ID: {0} - NAME: {1} - PRICE: {2} HUF - CORES: {3}", item.CPU_ID, item.CPU_NAME, item.PRICE, item.CORES, i);
                            i++;
                        }

                        Console.WriteLine("Which cpu would you like to update? (use ID to Identify)");
                        int cpuIndex = int.Parse(Console.ReadLine());
                        if (cpuLogic.GetOne(cpuIndex) != null)
                        {
                            Console.WriteLine("Would you like to modify its name, price or cores?");
                            string cpumodifier;
                            do
                            {
                                Console.Write("Choice: ");
                                cpumodifier = Console.ReadLine();
                            }
                            while (cpumodifier != "name" && cpumodifier != "price" && cpumodifier != "cores");
                            switch (cpumodifier)
                            {
                                case "name":
                                    Console.Write("What should be its new name?: ");
                                    string cpuNewName = Console.ReadLine();
                                    cpuLogic.UpdateName(cpuIndex, cpuNewName);
                                    break;
                                case "price":
                                    Console.Write("What should be its new price?: ");
                                    string cpuNewPrice = Console.ReadLine();
                                    cpuLogic.UpdatePrice(cpuIndex, int.Parse(cpuNewPrice));
                                    break;
                                case "type":
                                    Console.Write("What should be its new core amount?: ");
                                    string cpuNewCoreAmount = Console.ReadLine();
                                    cpuLogic.UpdateCores(cpuIndex, int.Parse(cpuNewCoreAmount));
                                    break;
                                default:
                                    break;
                            }
                        }
                        else
                        {
                            Console.WriteLine("You chose a wrong entity. Try again.");
                            UpdateEntity();
                        }

                        break;
                    case '4':
                        GPULogic gpuLogic = new GPULogic();
                        foreach (var item in gpuLogic.GetAll())
                        {
                            Console.WriteLine("\n[{4}] ID: {0} - NAME: {1} - PRICE: {2} HUF - MEMORY: {3}", item.GPU_ID, item.GPU_NAME, item.PRICE, item.RAM_SIZE, i);
                            i++;
                        }

                        Console.WriteLine("Which gpu would you like to update? (use ID to Identify) ");
                        int gpuIndex = int.Parse(Console.ReadLine());
                        if (!gpuLogic.GetOne(gpuIndex).Equals(null))
                        {
                            Console.WriteLine("Would you like to modify its name, price or VR property?");
                            string gpuModifier;
                            do
                            {
                                Console.Write("Choice: ");
                                gpuModifier = Console.ReadLine();
                            }
                            while (gpuModifier != "name" && gpuModifier != "price" && gpuModifier != "VR");
                            switch (gpuModifier)
                            {
                                case "name":
                                    Console.Write("What should be its new name?: ");
                                    string gpuNewName = Console.ReadLine();
                                    gpuLogic.UpdateName(gpuIndex, gpuNewName);
                                    break;
                                case "price":
                                    Console.Write("What should be its new price?: ");
                                    string gpuNewPrice = Console.ReadLine();
                                    gpuLogic.UpdatePrice(gpuIndex, int.Parse(gpuNewPrice));
                                    break;
                                case "VR":
                                    Console.Write("Should it be VR ready? (0 -> false, 1 -> True): ");
                                    string ramNewType = Console.ReadLine();
                                    gpuLogic.UpdateVR(gpuIndex, int.Parse(ramNewType));
                                    break;
                                default:
                                    break;
                            }
                        }
                        else
                        {
                            Console.WriteLine("You chose a wrong entity. Try again.");
                            UpdateEntity();
                        }

                        break;
                    case '5':
                        PSULogic psuLogic = new PSULogic();
                        foreach (var item in psuLogic.GetAll())
                        {
                            Console.WriteLine("\n[{4}] ID: {0} - NAME: {1} - PRICE: {2} HUF - WATTAGE: {3}", item.PSU_ID, item.PSU_NAME, item.PRICE, item.WATTAGE, i);
                            i++;
                        }

                        Console.WriteLine("Which psu would you like to update? (use ID to Identify)");
                        int psuIndex = int.Parse(Console.ReadLine());
                        if (!psuLogic.GetOne(psuIndex).Equals(null))
                        {
                            Console.WriteLine("Would you like to modify its name, price or efficiency?");
                            string psuModifier;
                            do
                            {
                                Console.Write("Choice: ");
                                psuModifier = Console.ReadLine();
                            }
                            while (psuModifier != "name" && psuModifier != "price" && psuModifier != "efficiency");
                            switch (psuModifier)
                            {
                                case "name":
                                    Console.Write("What should be its new name?: ");
                                    string psuNewName = Console.ReadLine();
                                    psuLogic.UpdateName(psuIndex, psuNewName);
                                    break;
                                case "price":
                                    Console.Write("What should be its new price?: ");
                                    string psuNewPrice = Console.ReadLine();
                                    psuLogic.UpdatePrice(psuIndex, int.Parse(psuNewPrice));
                                    break;
                                case "type":
                                    Console.Write("What should be its new efiiciency?: ");
                                    string psuNewEfficiency = Console.ReadLine();
                                    psuLogic.UpdateEfficiency(psuIndex, int.Parse(psuNewEfficiency));
                                    break;
                                default:
                                    break;
                            }
                        }
                        else
                        {
                            Console.WriteLine("You chose a wrong entity. Try again.");
                            UpdateEntity();
                        }

                        break;
                    default:
                        throw new InvalidCastException();
                }
            }
            catch (InvalidCastException)
            {
                Console.WriteLine("\nYou can choose only from 1 - 5.\nTry Again.");
                ReadEntities();
            }
        }

        private static void DeleteEntity()
        {
            Console.WriteLine("\n\nSelect a table to delete the content:");
            Console.WriteLine("Press 1 to delete one of the RAMs");
            Console.WriteLine("Press 2 to delete one of the Motherboards");
            Console.WriteLine("Press 3 to delete one of the CPUs");
            Console.WriteLine("Press 4 to delete one of the GPUs");
            Console.WriteLine("Press 5 to delete one of the PSUs");
            char tableIndex = Console.ReadKey().KeyChar;
            try
            {
                int i = 0;
                switch (tableIndex)
                {
                    case '1':
                        RAMLogic ramLogic = new RAMLogic();
                        foreach (var item in ramLogic.GetAll())
                        {
                            Console.WriteLine("\n[{4}] ID: {0} - NAME: {1} - PRICE: {2} HUF - SPEED: {3}", item.RAM_ID, item.RAM_NAME, item.PRICE, item.SPEED, i);
                            i++;
                        }

                        Console.WriteLine("Which ram would you like to delete? (use ID to Identify)");
                        int ramIndex = int.Parse(Console.ReadLine());
                        if (!ramLogic.GetOne(ramIndex).Equals(null))
                        {
                            ramLogic.DeleteItem(ramIndex);
                        }
                        else
                        {
                            Console.WriteLine("You chose a wrong entity. Try again.");
                            DeleteEntity();
                        }

                        break;
                    case '2':
                        MBLogic mbLogic = new MBLogic();
                        foreach (var item in mbLogic.GetAll())
                        {
                            Console.WriteLine("\n[{4}] ID: {0} - NAME: {1} - PRICE: {2} HUF - FORM: {3}", item.MD_ID, item.MB_NAME, item.PRICE, item.FORM_FACTOR, i);
                            i++;
                        }

                        Console.WriteLine("Which motherboard would you like to delete? (use ID to Identify)");
                        int mbIndex = int.Parse(Console.ReadLine());
                        if (!mbLogic.GetOne(mbIndex).Equals(null))
                        {
                            mbLogic.DeleteItem(mbIndex);
                        }
                        else
                        {
                            Console.WriteLine("You chose a wrong entity. Try again.");
                            DeleteEntity();
                        }

                        break;
                    case '3':
                        CPULogic cpuLogic = new CPULogic();
                        foreach (var item in cpuLogic.GetAll())
                        {
                            Console.WriteLine("\n[{4}] ID: {0} - NAME: {1} - PRICE: {2} HUF - CORES: {3}", item.CPU_ID, item.CPU_NAME, item.PRICE, item.CORES, i);
                            i++;
                        }

                        Console.WriteLine("Which cpu would you like to delete? (use ID to Identify)");
                        int cpuIndex = int.Parse(Console.ReadLine());
                        if (cpuLogic.GetOne(cpuIndex) != null)
                        {
                            cpuLogic.DeleteItem(cpuIndex);
                        }
                        else
                        {
                            Console.WriteLine("You chose a wrong entity. Try again.");
                            DeleteEntity();
                        }

                        break;
                    case '4':
                        GPULogic gpuLogic = new GPULogic();
                        foreach (var item in gpuLogic.GetAll())
                        {
                            Console.WriteLine("\n[{4}] ID: {0} - NAME: {1} - PRICE: {2} HUF - MEMORY: {3}", item.GPU_ID, item.GPU_NAME, item.PRICE, item.RAM_SIZE, i);
                            i++;
                        }

                        Console.WriteLine("Which gpu would you like to delete? (use ID to Identify) ");
                        int gpuIndex = int.Parse(Console.ReadLine());
                        if (!gpuLogic.GetOne(gpuIndex).Equals(null))
                        {
                            gpuLogic.DeleteItem(gpuIndex);
                        }
                        else
                        {
                            Console.WriteLine("You chose a wrong entity. Try again.");
                            DeleteEntity();
                        }

                        break;
                    case '5':
                        PSULogic psuLogic = new PSULogic();
                        foreach (var item in psuLogic.GetAll())
                        {
                            Console.WriteLine("\n[{4}] ID: {0} - NAME: {1} - PRICE: {2} HUF - WATTAGE: {3}", item.PSU_ID, item.PSU_NAME, item.PRICE, item.WATTAGE, i);
                            i++;
                        }

                        Console.WriteLine("Which psu would you like to delete? (use ID to Identify)");
                        int psuIndex = int.Parse(Console.ReadLine());
                        if (!psuLogic.GetOne(psuIndex).Equals(null))
                        {
                            psuLogic.DeleteItem(psuIndex);
                        }
                        else
                        {
                            Console.WriteLine("You chose a wrong entity. Try again.");
                            DeleteEntity();
                        }

                        break;
                    default:
                        throw new InvalidCastException();
                }
            }
            catch (InvalidCastException)
            {
                Console.WriteLine("\nYou can choose only from 1 - 5.\nTry Again.");
                DeleteEntity();
            }
        }

        private static void BuildPC()
        {
            MBLogic mbLog = new MBLogic();
            RAMLogic ramLog = new RAMLogic();
            PSULogic psuLog = new PSULogic();
            CPULogic cpuLog = new CPULogic();
            GPULogic gpuLog = new GPULogic();
            PCLogic pcLog = new PCLogic();

            Console.WriteLine("\n\nWelcome to the PC Builder! Lets start with the motherboard. Which one would you choose?");
            foreach (var item in mbLog.GetAll())
            {
                Console.WriteLine("\nID: {0} - NAME: {1} - PRICE: {2} HUF - FORM: {3}", item.MD_ID, item.MB_NAME, item.PRICE, item.FORM_FACTOR);
            }

            Console.Write("\nChoose by writing it's ID and press ENTER: ");
            int mbIdx = int.Parse(Console.ReadLine());
            if (mbLog.GetOne(mbIdx) == null)
            {
                Console.WriteLine("You chose a wrong entity! Program restart.");
                BuildPC();
            }

            Console.WriteLine("Now lets choose the RAM for your pc:");
            foreach (var item in ramLog.GetAll())
            {
                Console.WriteLine("\nID: {0} - NAME: {1} - PRICE: {2} HUF - SPEED: {3}Mhz", item.RAM_ID, item.RAM_NAME, item.PRICE, item.SPEED);
            }

            Console.Write("\nYour choice: ");
            int ramIdx = int.Parse(Console.ReadLine());
            if (ramLog.GetOne(ramIdx) == null)
            {
                Console.WriteLine("You chose a wrong entity! Program restart.");
                BuildPC();
            }

            Console.WriteLine("How many ram do you need? max amount: {0}", mbLog.GetOne(mbIdx).MEMORY_SLOTS);
            int ramAmount = int.Parse(Console.ReadLine());
            while (!(ramAmount > 0 && ramAmount <= mbLog.GetOne(mbIdx).MEMORY_SLOTS))
            {
                ramAmount = int.Parse(Console.ReadLine());
            }

            Console.WriteLine("Now lets choose the PSU for your pc:");
            foreach (var item in psuLog.GetAll())
            {
                Console.WriteLine("\nID: {0} - NAME: {1} - PRICE: {2} HUF - WATTAGE: {3}W", item.PSU_ID, item.PSU_NAME, item.PRICE, item.WATTAGE);
            }

            Console.Write("\nYour choice: ");
            int psuIdx = int.Parse(Console.ReadLine());
            if (psuLog.GetOne(psuIdx) == null)
            {
                Console.WriteLine("You chose a wrong entity! Program restart.");
                BuildPC();
            }

            Console.WriteLine("Now lets choose the CPU for your pc:");
            foreach (var item in cpuLog.GetAll())
            {
                Console.WriteLine("\nID: {0} - NAME: {1} - PRICE: {2} HUF - CORES: {3}", item.CPU_ID, item.CPU_NAME, item.PRICE, item.CORES);
            }

            int cpuIdx = int.Parse(Console.ReadLine());
            if (cpuLog.GetOne(cpuIdx) == null)
            {
                Console.WriteLine("You chose a wrong entity! Program restart.");
                BuildPC();
            }

            Console.WriteLine("Now lets choose the GPU for your pc:");
            foreach (var item in gpuLog.GetAll())
            {
                Console.WriteLine("\nID: {0} - NAME: {1} - PRICE: {2} HUF - MEMORY: {3}", item.GPU_ID, item.GPU_NAME, item.PRICE, item.RAM_SIZE);
            }

            int gpuIdx = int.Parse(Console.ReadLine());
            if (gpuLog.GetOne(gpuIdx) == null)
            {
                Console.WriteLine("You chose a wrong entity! Program restart.");
                BuildPC();
            }

            pcLog.AddPC(new PC_SWITCHTABLE()
            {
                PC_ID = 1111,
                MD_ID = mbIdx,
                PSU_ID = psuIdx,
                CPU_ID = cpuIdx,
                RAM_ID = ramIdx,
                RAM_AMOUNT = ramAmount,
                GPU_ID = gpuIdx,
            });

            Console.WriteLine("Build completed! This is what you have built:");
            Console.WriteLine("{0} - FORM: {1} - {2} {3} MEMORY SLOT", mbLog.GetOne(mbIdx).MB_NAME, mbLog.GetOne(mbIdx).FORM_FACTOR, mbLog.GetOne(mbIdx).MEMORY_SLOTS, mbLog.GetOne(mbIdx).MEMORY_TYPE);
            Console.WriteLine("{0} - SPEED: {1}Mhz - CL: {2}", ramLog.GetOne(ramIdx).RAM_NAME, ramLog.GetOne(ramIdx).SPEED, ramLog.GetOne(ramIdx).CL);
            Console.WriteLine("{0} - WATTAGE: {1}W - EFFICIENCY: {2}% - WEIGHT: {3} kg ", psuLog.GetOne(psuIdx).PSU_NAME, psuLog.GetOne(psuIdx).WATTAGE, psuLog.GetOne(psuIdx).EFFICIENCY, psuLog.GetOne(psuIdx).PSU_WEIGHT);
            Console.WriteLine("{0} - CORES: {1} - THREADS: {2} - CLOCKSPEED: {3}Mhz", cpuLog.GetOne(cpuIdx).CPU_NAME, cpuLog.GetOne(cpuIdx).CORES, cpuLog.GetOne(cpuIdx).THREADS, cpuLog.GetOne(cpuIdx).FREQUENCY);
            Console.WriteLine("{0} - MEMORY: {1}Gb   - SPEED {2}Mhz", gpuLog.GetOne(gpuIdx).GPU_NAME, gpuLog.GetOne(gpuIdx).RAM_SIZE, gpuLog.GetOne(gpuIdx).CLOCK_SPEED);
            Console.WriteLine("Total cost: {0} HUF", psuLog.GetOne(psuIdx).PRICE + (ramLog.GetOne(ramIdx).PRICE * ramAmount) + psuLog.GetOne(psuIdx).PRICE + cpuLog.GetOne(cpuIdx).PRICE + gpuLog.GetOne(gpuIdx).PRICE);
        }

        private static void AVGPrice()
        {
            MBLogic mbLog = new MBLogic();
            RAMLogic ramLog = new RAMLogic();
            PSULogic psuLog = new PSULogic();
            CPULogic cpuLog = new CPULogic();
            GPULogic gpuLog = new GPULogic();
            Console.WriteLine("\nThere are {0} Motherboards, Average price is: {1}", mbLog.GetAll().Count, mbLog.AVGPrice());
            Console.WriteLine("There are {0} RAMs, Average price is: {1}", ramLog.GetAll().Count, ramLog.AVGPrice());
            Console.WriteLine("There are {0} PSUs, Average price is: {1}", psuLog.GetAll().Count, psuLog.AVGPrice());
            Console.WriteLine("There are {0} CPUs, Average price is: {1}", cpuLog.GetAll().Count, cpuLog.AVGPrice());
            Console.WriteLine("There are {0} GPUs, Average price is: {1}", gpuLog.GetAll().Count, gpuLog.AVGPrice());
        }

        private static void Filtering()
        {
            MBLogic mbLog = new MBLogic();
            RAMLogic ramLog = new RAMLogic();
            GPULogic gpuLog = new GPULogic();

            Console.WriteLine("\nWhich preset filter would you like to choose?");
            Console.WriteLine("Press 1 to filter RAM by memory speed over a certain number");
            Console.WriteLine("Press 2 to filter motherboards by forms you choose");
            Console.WriteLine("Press 3 to filter GPUs by price between");
            char c = Console.ReadKey().KeyChar;
            switch (c)
            {
                case '1':
                    Console.Write("minimum memory speed: ");
                    int ramMinimumSpeed = int.Parse(Console.ReadLine());
                    List<RAM> filteredRAMs = ramLog.FilterBySpeed(ramMinimumSpeed);
                    foreach (var item in filteredRAMs)
                    {
                        Console.WriteLine("\nID: {0} - NAME: {1} - PRICE: {2} HUF - SPEED: {3}", item.RAM_ID, item.RAM_NAME, item.PRICE, item.SPEED);
                    }

                    break;
                case '2':
                    Console.Write("which form? (Choosable forms: ATX, E-ATX, M-ATX)");
                    string mbChosenForm = Console.ReadLine();
                    while (mbChosenForm != "ATX" && mbChosenForm != "E-ATX" && mbChosenForm != "M-ATX")
                    {
                        mbChosenForm = Console.ReadLine();
                    }

                    List<MOTHERBOARD> filteredMBs = mbLog.FilterByForm(mbChosenForm);
                    foreach (var item in filteredMBs)
                    {
                        Console.WriteLine("\nID: {0} - NAME: {1} - PRICE: {2} HUF - FORM: {3}", item.MD_ID, item.MB_NAME, item.PRICE, item.FORM_FACTOR);
                    }

                    break;
                case '3':
                    Console.Write("minimum price: ");
                    int gpuMinPrice = int.Parse(Console.ReadLine());
                    Console.Write("\nmaximum price: ");
                    int gpuMaxPrice = int.Parse(Console.ReadLine());
                    List<GPU> filteredGPUs = gpuLog.FilterPriceBetween(gpuMinPrice, gpuMaxPrice);
                    foreach (var item in filteredGPUs)
                    {
                        Console.WriteLine("\nID: {0} - NAME: {1} - PRICE: {2} HUF - MEMORY: {3}", item.GPU_ID, item.GPU_NAME, item.PRICE, item.RAM_SIZE);
                    }

                    break;
                default:
                    Console.WriteLine("Wrong choice. Program restarting...");
                    Filtering();
                    break;
            }
        }

        private static void JavaBuildPC()
        {
            MBLogic mbLog = new MBLogic();
            RAMLogic ramLog = new RAMLogic();
            PSULogic psuLog = new PSULogic();
            CPULogic cpuLog = new CPULogic();
            GPULogic gpuLog = new GPULogic();
            PCLogic pcl = new PCLogic();

            int mbMaxIdx = (int)mbLog.GetMaxIdx();
            int ramMaxIdx = (int)ramLog.GetMaxIdx();
            int psuMaxIdx = (int)psuLog.GetMaxIdx();
            int cpuMaxIdx = (int)cpuLog.GetMaxIdx();
            int gpuMaxIdx = (int)gpuLog.GetMaxIdx();
            string url = "http://localhost:8080/RandomizeBuild/Randomize?cpuid="
                + cpuMaxIdx + "&gpuid=" + gpuMaxIdx + "&psuid=" + psuMaxIdx + "&mbid=" + mbMaxIdx + "&ramid=" + ramMaxIdx;
            XDocument xDoc = XDocument.Load(url);
            int ramAmount = R.Next((int)mbLog.GetOne(int.Parse(xDoc.Descendants("mbid").First().Value)).MEMORY_SLOTS.Value);
            pcl.AddPC(new PC_SWITCHTABLE()
            {
                PC_ID = R.Next(1112, 9999),
                MD_ID = decimal.Parse(xDoc.Descendants("mbid").First().Value),
                RAM_ID = decimal.Parse(xDoc.Descendants("ramid").First().Value),
                RAM_AMOUNT = ramAmount,
                PSU_ID = decimal.Parse(xDoc.Descendants("psuid").First().Value),
                CPU_ID = decimal.Parse(xDoc.Descendants("cpuid").First().Value),
                GPU_ID = decimal.Parse(xDoc.Descendants("gpuid").First().Value),
            });
            int mbIdx = int.Parse(xDoc.Descendants("mbid").First().Value);
            int ramIdx = int.Parse(xDoc.Descendants("ramid").First().Value);
            int psuIdx = int.Parse(xDoc.Descendants("psuid").First().Value);
            int cpuIdx = int.Parse(xDoc.Descendants("cpuid").First().Value);
            int gpuIdx = int.Parse(xDoc.Descendants("gpuid").First().Value);
            Console.WriteLine("\n\nBuild completed! This is what you randomized:");

            Console.WriteLine("{0} - FORM: {1} - {2} {3} MEMORY SLOT", mbLog.GetOne(mbIdx).MB_NAME, mbLog.GetOne(mbIdx).FORM_FACTOR, mbLog.GetOne(mbIdx).MEMORY_SLOTS, mbLog.GetOne(mbIdx).MEMORY_TYPE);
            Console.WriteLine("{0} - SPEED: {1}Mhz - CL: {2}", ramLog.GetOne(ramIdx).RAM_NAME, ramLog.GetOne(ramIdx).SPEED, ramLog.GetOne(ramIdx).CL);
            Console.WriteLine("{0} - WATTAGE: {1}W - EFFICIENCY: {2}% - WEIGHT: {3} kg ", psuLog.GetOne(psuIdx).PSU_NAME, psuLog.GetOne(psuIdx).WATTAGE, psuLog.GetOne(psuIdx).EFFICIENCY, psuLog.GetOne(psuIdx).PSU_WEIGHT);
            Console.WriteLine("{0} - CORES: {1} - THREADS: {2} - CLOCKSPEED: {3}Mhz", cpuLog.GetOne(cpuIdx).CPU_NAME, cpuLog.GetOne(cpuIdx).CORES, cpuLog.GetOne(cpuIdx).THREADS, cpuLog.GetOne(cpuIdx).FREQUENCY);
            Console.WriteLine("{0} - MEMORY: {1}Gb   - SPEED {2}Mhz", gpuLog.GetOne(gpuIdx).GPU_NAME, gpuLog.GetOne(gpuIdx).RAM_SIZE, gpuLog.GetOne(gpuIdx).CLOCK_SPEED);
            Console.WriteLine("Total cost: {0} HUF", psuLog.GetOne(psuIdx).PRICE + (ramLog.GetOne(ramIdx).PRICE * ramAmount) + psuLog.GetOne(psuIdx).PRICE + cpuLog.GetOne(cpuIdx).PRICE + gpuLog.GetOne(gpuIdx).PRICE);
        }
    }
}