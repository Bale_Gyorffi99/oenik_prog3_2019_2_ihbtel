﻿// <copyright file="CRUDTests.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Pre_BuiltPCs.Logic.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Moq;
    using NUnit.Framework;
    using Pre_BuiltPCs;
    using Pre_BuiltPCs.Data;
    using Pre_BuiltPCs.Repository;
    using Assert = NUnit.Framework.Assert;

    /// <summary>
    /// Class, that contains every CRUD test.
    /// </summary>
    [TestFixture]
    public class CRUDTests
    {
        private Mock<IMBRepository> mbMockRepo;
        private Mock<IRAMRepository> ramMockRepo;
        private Mock<IGPURepository> gpuMockRepo;
        private Mock<ICPURepository> cpuMockRepo;
        private Mock<IPSURepository> psuMockRepo;

        private MBLogic mbController;
        private RAMLogic ramController;
        private CPULogic cpuController;
        private PSULogic psuController;
        private GPULogic gpuController;

        /// <summary>
        /// Created Mocked repository and sets up .GetAll() function.
        /// </summary>
        [SetUp]
        public void Init()
        {
            this.mbMockRepo = new Mock<IMBRepository>();
            this.ramMockRepo = new Mock<IRAMRepository>();
            this.gpuMockRepo = new Mock<IGPURepository>();
            this.cpuMockRepo = new Mock<ICPURepository>();
            this.psuMockRepo = new Mock<IPSURepository>();

            this.mbMockRepo.Setup(x => x.GetAll()).Returns(new List<MOTHERBOARD>()
            {
                new MOTHERBOARD()
                {
                MB_NAME = "Alaplap1", FORM_FACTOR = "ATX", MD_ID = 0111, MEMORY_SLOTS = 4, MEMORY_TYPE = "GDDR4", PRICE =
                25000, SERIALNUMBER = 19283412,
                },
                new MOTHERBOARD()
                {
                MB_NAME = "Alaplap2", FORM_FACTOR = "M-ATX", MD_ID = 0112, MEMORY_SLOTS = 2, MEMORY_TYPE = "GDDR3", PRICE =
                15000, SERIALNUMBER = 19283321,
                },
                new MOTHERBOARD()
                {
                MB_NAME = "Alaplap1", FORM_FACTOR = "E-ATX", MD_ID = 0113, MEMORY_SLOTS = 2, MEMORY_TYPE = "GDDR3", PRICE =
                20000, SERIALNUMBER = 19281234,
                },
            });

            this.mbController = new MBLogic(this.mbMockRepo.Object);

            this.ramMockRepo.Setup(x => x.GetAll()).Returns(new List<RAM>()
            {
                new RAM()
                {
                    RAM_NAME = "Ram1", CL = 14, PRICE = 15000, RAM_ID = 0111, RAM_SIZE = 16, RAM_TYPE = "DDR4", SERIALNUMBER =
                    12831723, SPEED = 3000,
                },
                new RAM()
                {
                    RAM_NAME = "Ram2", CL = 16, PRICE = 10000, RAM_ID = 0112, RAM_SIZE = 8, RAM_TYPE = "DDR3", SERIALNUMBER =
                    12831113, SPEED = 3000,
                },
                new RAM()
                {
                    RAM_NAME = "Ram3", CL = 13, PRICE = 20000, RAM_ID = 0113, RAM_SIZE = 32, RAM_TYPE = "DDR5", SERIALNUMBER =
                    12221733, SPEED = 4000,
                },
            });

            this.ramController = new RAMLogic(this.ramMockRepo.Object);

            this.gpuMockRepo.Setup(x => x.GetAll()).Returns(new List<GPU>()
            {
                new GPU()
                {
                    GPU_NAME = "vidkari1", VR_READY = 1, CLOCK_SPEED = 1827, RAM_SIZE = 6, GPU_ID = 0111, PRICE = 80000, SERIALNUMBER =
                    19823452,
                },
                new GPU()
                {
                    GPU_NAME = "vidkari2", VR_READY = 0, CLOCK_SPEED = 1600, RAM_SIZE = 4, GPU_ID = 0112, PRICE = 50000, SERIALNUMBER =
                    19321452,
                },
                new GPU()
                {
                    GPU_NAME = "vidkari3", VR_READY = 1, CLOCK_SPEED = 1800, RAM_SIZE = 8, GPU_ID = 0113, PRICE = 100000, SERIALNUMBER =
                    19822452,
                },
            });

            this.gpuController = new GPULogic(this.gpuMockRepo.Object);

            this.cpuMockRepo.Setup(x => x.GetAll()).Returns(new List<CPU>()
            {
                new CPU()
                {
                    CPU_NAME = "processzor1", CORES = 8, FREQUENCY = 3600, PRICE = 85000, SOCKET = "AMD", SERIALNUMBER =
                    19264869, CPU_ID = 0111, THREADS = 16,
                },
                new CPU()
                {
                    CPU_NAME = "processzor2", CORES = 4, FREQUENCY = 3200, PRICE = 50000, SOCKET = "INTEL", SERIALNUMBER =
                    19211169, CPU_ID = 0112, THREADS = 8,
                },
                new CPU()
                {
                    CPU_NAME = "processzor3", CORES = 12, FREQUENCY = 3400, PRICE = 250000, SOCKET = "AMD", SERIALNUMBER =
                    19224869, CPU_ID = 0112, THREADS = 24,
                },
            });

            this.cpuController = new CPULogic(this.cpuMockRepo.Object);

            this.psuMockRepo.Setup(x => x.GetAll()).Returns(new List<PSU>()
            {
                new PSU()
                {
                    PSU_NAME = "táp1", EFFICIENCY = 78, PRICE = 20000, PSU_ID = 0111, PSU_WEIGHT = 2, SERIALNUMBER =
                    18245621, WATTAGE = 500,
                },
                new PSU()
                {
                    PSU_NAME = "táp2", EFFICIENCY = 88, PRICE = 40000, PSU_ID = 0112, PSU_WEIGHT = 3, SERIALNUMBER =
                    18243321, WATTAGE = 900,
                },
                new PSU()
                {
                    PSU_NAME = "táp3", EFFICIENCY = 92, PRICE = 86000, PSU_ID = 0113, PSU_WEIGHT = 4, SERIALNUMBER =
                    11145621, WATTAGE = 900,
                },
            });

            this.psuController = new PSULogic(this.psuMockRepo.Object);
        }

        /// <summary>
        /// Tests creating a new PSU.
        /// </summary>
        [Test]
        public void Test_CreateNewPSU()
        {
            PSU newPSU = new PSU()
            {
                PSU_NAME = "táp4",
                EFFICIENCY = 72,
                PRICE = 15000,
                PSU_ID = 0114,
                PSU_WEIGHT = 1,
                SERIALNUMBER = 13122621,
                WATTAGE = 400,
            };
            this.psuController.CreateItem(newPSU);

            this.psuMockRepo.Verify(x => x.CreateItem(It.IsAny<PSU>()), Times.Once());

            // Assert.That(psuController.GetAll().Count == 4);
        }

        /// <summary>
        /// Tests deleting a CPU.
        /// </summary>
        [Test]
        public void Test_DeleteItemCPU()
        {
            this.cpuController.DeleteItem(2);

            this.cpuMockRepo.Verify(x => x.DeleteItem(It.IsAny<int>()), Times.Once());

            // Assert.That(cpuController.GetAll().Count == 2);
        }

        /// <summary>
        /// Tests if modifying works (I used Motherboard as a test subject,).
        /// </summary>
        [Test]
        public void Test_ModifyMBPrice()
        {
            this.mbController.UpdatePrice(112, 60000);

            this.mbController.UpdatePrice(112, 40000);

            this.mbMockRepo.Verify(x => x.UpdatePrice(It.IsAny<int>(), It.IsAny<int>()), Times.Exactly(2));
        }

        /// <summary>
        /// Tests if .GetAll() method works.
        /// </summary>
        [Test]
        public void Test_ReadAllRAM()
        {
            Assert.That(this.ramController.GetAll().Count.Equals(3));
            this.ramMockRepo.Verify(x => x.GetAll(), Times.Once);
        }

        /// <summary>
        /// Tests if .GetOne() method runs.
        /// </summary>
        [Test]
        public void Test_ReadOneRAM()
        {
            RAM item = this.ramController.GetOne(0111);
            this.ramMockRepo.Verify(x => x.GetOne(It.IsAny<int>()), Times.Once);
        }
    }
}