/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Random;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Bale
 */
@WebServlet(name = "Randomize", urlPatterns = {"/Randomize"})
public class Randomize extends HttpServlet {
    static Random r =new Random();
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/xml;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            int cpuID = Integer.parseInt(request.getParameter("cpuid"));
            int gpuID = Integer.parseInt(request.getParameter("gpuid"));
            int psuID = Integer.parseInt(request.getParameter("psuid"));
            int mbID = Integer.parseInt(request.getParameter("mbid"));
            int ramID = Integer.parseInt(request.getParameter("ramid"));
//            if (cpuID == 0) {
//                cpuID = 1112;
//            }
//            if (gpuID == 0) {
//                gpuID = 1112;
//            }
//            if (psuID == 0) {
//                psuID = 1112;
//            }
//            if (mbID == 0) {
//                mbID = 1112;
//            }
//            if (ramID == 0) {
//                ramID = 1112;
//            }
            out.println("<Randomize>");
            out.println("<cpuid>" + (r.nextInt(cpuID-1110)+1111) + "</cpuid>");
            out.println("<gpuid>" + (r.nextInt(gpuID-1110)+1111) + "</gpuid>");
            out.println("<psuid>" + (r.nextInt(psuID-1110)+1111) + "</psuid>");
            out.println("<mbid>" + (r.nextInt(mbID-1110)+1111) + "</mbid>");
            out.println("<ramid>" + (r.nextInt(ramID-1110)+1111) + "</ramid>");
            out.println("</Randomize>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
